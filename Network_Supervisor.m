function Network_Supervisor(ind_sim)

UE_den_tot=  str2num(getenv('UEDENSITY'));
BS_den    =  str2num(getenv('BSDENSITY'));
Noise_db  =  str2num(getenv('NOISE'));
theta_deg =  str2num(getenv('THETA'));
percent   =  str2num(getenv('PERCENT'));  % Market Share of ISP1
flag1     =  getenv('FLAG1');
flag3     =  getenv('FLAG3');
flag4     =  getenv('FLAG4');
flag5     =  getenv('FLAG5');
Sim_win   =  str2num(getenv('SIMWINDOW'));
n_ofts    =  str2num(getenv('NOFTS'));    % Scheduling share of ISP1 in OFTS
n_prs     =  str2num(getenv('NPRS'));     % Scheduling share of ISP1 in PRS
frf       =  str2num(getenv('FRF'));      % frequency reuse factor, i.e. total number of spectrum channels

%% General Parameters: Cellular system type, Infrastructure deployment policy, Spectrum management policy

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Flags: flag1, flag2, flag3, flag4, flag5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                                                                                                 %%%
%%%    flag1: Cellular system type: 		         - '00': 73 GHz mmWave band                     %%%
%%%                                  		         - '01': 28 GHz mmWave band                     %%%
%%%                                                  - '10': 2.5 GHz UMi (ITU standard)             %%%
%%%                                                                                                 %%%
%%%    flag3('abcdefgh'): Spectrum management policy:- 'a': Full Spectrum Sharing                   %%%
%%%                                                  - 'b': Even Spectrum Partitioning (ESP)        %%%
%%%                                                  - 'c': Proportional Spectrum Partitioning (PSP)%%%
%%%                                                  - 'd': Partial Spectrum Sharing (PSS_25)       %%%
%%%                                                  - 'e': Partial Spectrum Sharing (PSS_50)       %%%
%%%                                                  - 'f': Partial Spectrum Sharing (PSS_75)       %%%
%%%                                           	     - 'g': Opportunistic Fair Time Sharing (OFTS)  %%% 
%%%                                                  - 'h': Proportional Resource Sharing(PRS)      %%%
%%%                                                                                                 %%%
%%%    flag4: Network visualization:                 - '0': Off                                     %%%
%%%                                                  - '1': On                                      %%%
%%%                                                                                                 %%%
%%%    flag5: Antenna parameters                     - '0': BS antenna: Lowly directional           %%%
%%%                                                         UE antenna: Omnidirectional             %%%
%%%                                                                                                 %%%
%%%                                                  - '1': BS antenna: Highly directional          %%%
%%%                                                         UE antenna: Highly directional          %%%
%%%                                                                                                 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T_slot=1e5;                              % Number of time slots
N_ISP=2;                                 % Number of ISPs
N_BS_tot=ceil((Sim_win^2)*1e-6*BS_den);  % Average number of BSs in the simulation window
MarketS=[percent,1-percent];             % Market share of the ISPs
N_BS(1)=floor(N_BS_tot*MarketS(1));      % Average number of BSs of ISPs
N_BS(2)=floor(N_BS_tot*MarketS(2));
N_UE(1)=floor(UE_den_tot*MarketS(1));    % Average number of UEs of ISPs
N_UE(2)=floor(UE_den_tot*MarketS(2));
p=MarketS(2)/MarketS(1);                 % Probability of having a BS for ISP2 near the BS of ISP1 (in clustered mode for BS deployment)
Noise = 10^(Noise_db/10)/1000.0;         % dB to linear conversion
theta_min = pi/180*theta_deg;            % Separation angle in radians
d_grid= Sim_win./100;                    % Adjacent point distance in the grid of the netwrk
r_grid= d_grid;                          % Radius of the circle in UE distribution procedure
Threshold_int=250;			             % Distance within which we compute interference

% Ignore these; they correspond to part of the code that was not fully implemented
flag2='0';
r_clus=10;				 

%% Channel Modeling Parameters

if strcmp(flag1,'00')     % mmWave band 1 -- 73 GHZ
    
    c=3e8;                % Light velocity in vacuum
    fc=73;                % mmWave frequency 1 in GHz
    lam=c/(fc*1e9);       % Wave length in meters
    P_transmit_tot=1./frf;% Total transmitting power at the base station is 30 dBm which spreads over the whole bandwidth
    Noise=Noise./frf;     % Total noise power in each frequency channel
    alpha_LOS=69.8;       % Pathloss parameter 1 for LOS
    alpha_NLOS=86.6;      % Pathloss parameter 1 for NLOS
    beta_LOS=2;           % Pathloss parameter 2 for LOS
    beta_NLOS=2.45;       % Pathloss parameter 2 for NLOS
    sigma_LOS=5.8;        % Shadowing standard variation for LOS
    sigma_NLOS=8;         % Shadowing standard variation for NLOS
    aout=1/30;            % Parameter of the outage probability
    bout=5.2;             % Parameter of the outage probability
    aLOS=1/67.1;          % Parameter of the LOS probability
    
    plLOS  = @(d) alpha_LOS+10*beta_LOS*log10(d);                    % Average pathloss function for LOS UEs
    plNLOS = @(d) alpha_NLOS+10*beta_NLOS*log10(d);                  % Average pathloss function for NLOS UEs
    PrOUT  = @(d) max(0,1-exp((-aout.*d)+bout));                     % Probability of bening in outage for a UE
    PrLOS  = @(d) (1-max(0,1-exp((-aout.*d)+bout))).*exp(-aLOS.*d);  % Probability of bening an LOS UE
    
    if strcmp(flag5,'0')  % Worst case for antenna pattern
        Na_BS        = 10;                                           % Number of antennas at BS
        side_lobe_BS = 1 ;                                           % Side-lobe level
        beam_Ant_BS  = floor(360/theta_deg)*45*(pi/180);             % Beamwidth of the antenna pattern in radian
        Na_UE        = 1;                                            % Number of antennas at UE
        side_lobe_UE = 1;                                            % Side-lobe level
        beam_Ant_UE  = 360*(pi/180);                                 % Beamwidth of the antenna pattern in radian
    else                  % Best case for antenna pattern
        Na_BS        = 100;                                          % Number of antennas at BS
        side_lobe_BS = 0.1;                                          % Side-lobe level
        beam_Ant_BS  = floor(360/theta_deg)*5*(pi/180);              % Beamwidth of the antenna pattern in radian
        Na_UE        = 10;                                           % Number of antennas at UE
        side_lobe_UE = 0.1;                                          % Side-lobe level
        beam_Ant_UE  = 30*(pi/180);                                  % Beamwidth of the antenna pattern in radian
    end
    
elseif strcmp(flag1,'01') % mmWave band 2 -- 28 GHz
    
    c=3e8;                % Light velocity in vacuum
    fc=28;                % mmWave frequency 2 in GHz
    lam=c/(fc*1e9);       % Wave length in meters
    P_transmit_tot=1./frf;% Total transmitting power at the base station is 30 dBm which spreads over the whole bandwidth
    Noise=Noise./frf;     % Total noise power in each frequency channel
    alpha_LOS=61.4;       % Pathloss parameter 1 for LOS
    alpha_NLOS=72;        % Pathloss parameter 1 for NLOS
    beta_LOS=2;           % Pathloss parameter 2 for LOS
    beta_NLOS=2.92;       % Pathloss parameter 2 for NLOS
    sigma_LOS=5.8;        % Shadowing standard variation for LOS
    sigma_NLOS=8.7;       % Shadowing standard variation for NLOS
    aout=1/30;            % Parameter of the outage probability
    bout=5.2;             % Parameter of the outage probability
    aLOS=1/67.1;          % Parameter of the LOS probability
    
    
    plLOS  = @(d) alpha_LOS+ 10*beta_LOS*log10(d);                   % Average pathloss function for LOS UEs
    plNLOS = @(d) alpha_NLOS+ 10*beta_NLOS*log10(d);                 % Average pathloss function for NLOS UEs
    PrOUT  = @(d) max(0,1-exp((-aout.*d)+bout));                     % Probability of bening in outage for a UE
    PrLOS  = @(d) (1-max(0,1-exp((-aout.*d)+bout))).*exp(-aLOS.*d);  % Probability of bening an LOS UE
    
    
    if strcmp(flag5,'0')  % Worst case for antenna pattern
        Na_BS        = 10;                                           % Number of antennas at BS
        side_lobe_BS = 1 ;                                           % Side-lobe level
        beam_Ant_BS  = floor(360/theta_deg)*45*(pi/180);             % Beamwidth of the antenna pattern in radian
        Na_UE        = 1;                                            % Number of antennas at UE
        side_lobe_UE = 1;                                            % Side-lobe level
        beam_Ant_UE  = 360*(pi/180);                                 % Beamwidth of the antenna pattern in radian
    else                  % Best case for antenna pattern
        Na_BS        = 100;                                          % Number of antennas at BS
        side_lobe_BS = 0.1;                                          % Side-lobe level
        beam_Ant_BS  = floor(360/theta_deg)*5*(pi/180);              % Beamwidth of the antenna pattern in radian
        Na_UE        = 10;                                           % Number of antennas at UE
        side_lobe_UE = 0.1;                                          % Side-lobe level
        beam_Ant_UE  = 30*(pi/180);                                  % Beamwidth of the antenna pattern in radian
    end
    
elseif strcmp(flag1,'10') % 3GPP UMi -- 2.5 GHz
    
    c=3e8;                                % Light velocity in vacuum
    fc=2.5;                               % 3GPP UMi frequency in GHz
    lam=c/(fc*1e9);                       % Wave length in meters

    %Na_BS=1;                              % Number of antennas at the base station
    %BSAntGain=17;                         % BS antenna gain in dBi
    %P_transmit_tot=1*(10^(BSAntGain/10)); % Total transmitting power at the base station 30 dBm
    
    P_transmit_tot=1./frf;                % Total transmitting power at the base station is 30 dBm which spreads over the whole bandwidth
    Noise=Noise./frf;                     % Total noise power in each frequency channel
    R_cell=1000;                          % Cell radius in meters
    hbs = 10;                             % BS antenna height in meters
    hut = 1.5;                            % UT antenna height in meters
    hbui=20;                              % Average height of the buildings in meters
    wst=20;                               % Street width in meters
    
    % Compute breakpoint from the footnote under Table B.1.2.1-1
    hbseff = hbs-1;                       % Effective antenna heights
    huteff = hut-1;
    dbp = 4*hbseff*huteff*fc*1e9/c;       % Breakpoint distance
    
    alpha_LOS=28.0 + 20*log10(fc);        % Pathloss parameter 1 for LOS
    %alpha_NLOS=161.04-7.1*log10(wst)+7.5*log10(hbui)-((24.37-3.7*(hbui/hbs)^2)*log10(hbs))-((43.42-3.1*log10(hbs))*3)+20*log10(fc)-(3.2*(log10(11.75*hut))^2-4.97);% Pathloss parameter 1 for NLOS
    alpha_NLOS=22.7+26*log10(fc);
    beta_LOS=2.2;                         % Pathloss parameter 2 for LOS
    %beta_NLOS=(43.42-3.1*log10(hbs))./10; % Pathloss parameter 2 for NLOS
    beta_NLOS=3.67;
    sigma_LOS=3;                          % Shadowing standard variation for LOS
    sigma_NLOS=4;                         % Shadowing standard variation for NLOS
    
    
    plLOS1 = @(d) alpha_LOS+10*beta_LOS*log10(d) ;   % d < dbp
    plLOS2 = @(d) 40*log10(d) + 7.8 - 18*log10(hbseff) - 18*log10(huteff) + 2*log10(fc); % d> dbp
    plLOS  = @(d) plLOS1(d).*(d < dbp) + plLOS2(d).*(d >= dbp); % Average pathloss function for LOS UEs
    plNLOS = @(d) alpha_NLOS+10*beta_NLOS*log10(d);             % Average pathloss function for NLOS UEs
    PrOUT  = @(d) zeros(length(d),1);                           % Probability of bening in outage for a UE
    PrLOS  = @(d) (min(18./d,1).*(1-exp(-d./36)))+exp(-d./36);  % Probability of bening an LOS UE
    
    
    Na_BS        = 1;                                            % Number of antennas at BS
    side_lobe_BS = 0.01 ;                                        % Side-lobe level
    beam_Ant_BS  = floor(360/theta_deg)*70*(pi/180);             % Beamwidth of the antenna pattern in radian
    Na_UE        = 1;                                            % Number of antennas at UE
    side_lobe_UE = 1;                                            % Side-lobe level
    beam_Ant_UE  = 360*(pi/180);
    
end

% %% Channel Modeling Figures
% % Pathloss model
% d = logspace(0,3,1000)';     % Distance d in meters
% figure;
% semilogx(d,[plLOS(d) plNLOS(d)],'-', 'Linewidth', 2);
% grid on;
% set(gca, 'Fontsize', 16);
% xlabel('TR Separation (m)');
% ylabel('Path loss (dB)');
% legend('LOS','NLOS', 'Location', 'NorthWest');
%
% % Outage, LOS and NLOS probabilitie
% d = logspace(0,3,1000)';     % Distance d in meters
% figure;
% semilogx(d,[PrOUT(d) PrLOS(d) 1-PrLOS(d)-PrOUT(d)],'-', 'Linewidth', 2);
% grid on;
% set(gca, 'Fontsize', 16);
% xlabel('TR Separation (m)');
% ylabel('Probability');
% legend('PrOUT','PrLOS','PrNLOS', 'Location', 'NorthWest');


%% Rate Modeling Parameters: R=W*max{(1-OvheadLoss)*log2(1+Lossfact*SNR),ro_max}
OvheadLoss=0.2;   % Overhead loss
Lossfact=0.5;     % Loss factor in rate-SNR model
ro_max=1000;      % Spectral efficiency sauration value in bits/second/Hz

%% Base Station Deployment

notOctave = exist('OCTAVE_VERSION', 'builtin') == 0;
if notOctave
   rng('shuffle')  % Initialize the seeds to the clock of the system if the code is run on MATLAB
end
        
[N,BS_Locx,BS_Locy,BS_ISP_share,BS_ISP_part]=BS_Deployment(flag2,N_ISP,r_clus,N_BS,p,Sim_win);

%% User Drop
[n,UE_Locx,UE_Locy,UE_ISP]=User_Drop(flag2,N_ISP,N,N_UE,Sim_win,BS_Locx,BS_Locy,d_grid,r_grid);

%% UE-BS Association
[TargetBS_id_share,TargetBS_id_part_1,TargetBS_id_part_2,TargetBSx_share,TargetBSx_part_1,TargetBSx_part_2,TargetBSy_share,TargetBSy_part_1,TargetBSy_part_2,phi_targetBS_share,phi_targetBS_part_1,phi_targetBS_part_2,ISP_number_targetBS_share,ISP_number_targetBS_part_1,ISP_number_targetBS_part_2,User_id_targetBS_share,User_id_targetBS_part_1,User_id_targetBS_part_2,N_user_targetBS_share,N_user_targetBS_part_1,N_user_targetBS_part_2,d_ue_bs,UE_BS_share,UE_BS_part,D_UE_BS_share,D_UE_BS_part,Phi_UE_BS_share,Phi_UE_BS_part,K_share,K_part,ue_isp_bs_share,ue_isp_bs_part]=UE_BS_Association(n,N,UE_ISP,BS_Locx,BS_Locy,UE_Locx,UE_Locy,BS_ISP_share,BS_ISP_part);

%% Network Visualization
if strcmp(flag4,'1')
    Network_Visualization(N,TargetBSx_share,TargetBSx_part_1,TargetBSx_part_2,TargetBSy_share,TargetBSy_part_1,TargetBSy_part_2,TargetBS_id_share,TargetBS_id_part_1,TargetBS_id_part_2,BS_Locx,BS_Locy,BS_ISP_share,BS_ISP_part,n,UE_Locx,UE_Locy,UE_ISP,UE_BS_share,UE_BS_part,Sim_win);
end

%% Interference calculation

    % giving BSs random labels to select a part of frequency for them to work on (Frequency Reuse-n) 
    label_sp_1=[randi([1 frf],1,N(1)),zeros(1,N(2));zeros(1,sum(N))];                                               % spectrum partitioning case for ISP1 
    label_sp_2=[zeros(1,sum(N));zeros(1,N(1)),randi([frf+1 2*frf],1,N(2))];                                         % proportional spectrum partitioning case for ISP2
    label_ss=randi([1 frf],1,sum(N));                                                                               % Spectrum sharing case for BS sharing case
    label_sp=[label_sp_1(1,1:N(1)),randi([1 frf],1,N(2));randi([frf+1 2*frf],1,N(1)),label_sp_2(2,N(1)+1:sum(N))];  % proportional spectrum partitioning case for BS sharing case

    % With BS sharing
    for i=1:N_user_targetBS_share % Average interference is calculated for each user under different spectrum management strategies
        id=User_id_targetBS_share(i);
        [int_share_fss(i,:),int_share_esp(i,:),int_share_psp(i,:),int_share_ofts(i,:)]= Interference_Calculator(id,N,MarketS,UE_Locx(id),UE_Locy(id),BS_Locx,BS_Locy,Phi_UE_BS_share,K_share,UE_BS_share,UE_ISP,beam_Ant_BS,beam_Ant_UE,side_lobe_BS,side_lobe_UE,theta_deg,P_transmit_tot,flag3,Na_BS,Na_UE,T_slot,(d_ue_bs(id,:))',PrLOS,PrOUT,plLOS,plNLOS,sigma_LOS,sigma_NLOS,Threshold_int,label_sp,label_ss,TargetBS_id_share,n_ofts,n_prs);
    end                                                                                                                  
    
    % Without BS sharing ISP1
    for i=1:N_user_targetBS_part_1 % Average interference is calculated for each user under different spectrum management strategies
        id=User_id_targetBS_part_1(i);
        [int_part_1_fss(i,:),int_part_1_esp(i,:),int_part_1_psp(i,:),int_part_1_ofts(i,:)]= Interference_Calculator(id,N,MarketS,UE_Locx(id),UE_Locy(id),BS_Locx,BS_Locy,Phi_UE_BS_part,K_part,UE_BS_part,UE_ISP,beam_Ant_BS,beam_Ant_UE,side_lobe_BS,side_lobe_UE,theta_deg,P_transmit_tot,flag3,Na_BS,Na_UE,T_slot,(d_ue_bs(id,:))',PrLOS,PrOUT,plLOS,plNLOS,sigma_LOS,sigma_NLOS,Threshold_int,label_sp_1,label_ss,TargetBS_id_part_1,n_ofts,n_prs);
    end
    
    % Without BS sharing ISP2
    for i=1:N_user_targetBS_part_2 % Average interference is calculated for each user under different spectrum management strategies
        id=User_id_targetBS_part_2(i);
        [int_part_2_fss(i,:),int_part_2_esp(i,:),int_part_2_psp(i,:),int_part_2_ofts(i,:)]= Interference_Calculator(id,N,MarketS,UE_Locx(id),UE_Locy(id),BS_Locx,BS_Locy,Phi_UE_BS_part,K_part,UE_BS_part,UE_ISP,beam_Ant_BS,beam_Ant_UE,side_lobe_BS,side_lobe_UE,theta_deg,P_transmit_tot,flag3,Na_BS,Na_UE,T_slot,(d_ue_bs(id,:))',PrLOS,PrOUT,plLOS,plNLOS,sigma_LOS,sigma_NLOS,Threshold_int,label_sp_2,label_ss,TargetBS_id_part_2,n_ofts,n_prs);
    end


% Channel modeling and realization

% Generating channel coefficients of the users of target BS
[H_share]=Channel_Realization(N_user_targetBS_share,T_slot,D_UE_BS_share(User_id_targetBS_share,:),UE_BS_share(User_id_targetBS_share,:),PrLOS,PrOUT,plLOS,plNLOS,sigma_LOS,sigma_NLOS);
[H_part_1]=Channel_Realization(N_user_targetBS_part_1,T_slot,D_UE_BS_part(User_id_targetBS_part_1,:),UE_BS_part(User_id_targetBS_part_1,:),PrLOS,PrOUT,plLOS,plNLOS,sigma_LOS,sigma_NLOS);
[H_part_2]=Channel_Realization(N_user_targetBS_part_2,T_slot,D_UE_BS_part(User_id_targetBS_part_2,:),UE_BS_part(User_id_targetBS_part_2,:),PrLOS,PrOUT,plLOS,plNLOS,sigma_LOS,sigma_NLOS);

for i=1:N_user_targetBS_share % unifying the channel coefficients for the common users
    if sum(User_id_targetBS_part_1==User_id_targetBS_share(i))==1
        H_part_1(User_id_targetBS_part_1==User_id_targetBS_share(i),:)=H_share(i,:);
    end
    if sum(User_id_targetBS_part_2==User_id_targetBS_share(i))==1
        H_part_2(User_id_targetBS_part_2==User_id_targetBS_share(i),:)=H_share(i,:);
    end
end



%% BS procedure for the target BS

% BS target in BS sharing case
if sum(K_share(TargetBS_id_share,:))>0
    ISP_flag='0'; % BS sharing case (Full sharing, proportional resource sharing and BS sharing only cases)
    BS_Supervisor(ind_sim,MarketS,TargetBS_id_share,H_share,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE,P_transmit_tot,Noise,theta_min,OvheadLoss,Lossfact,ro_max,T_slot,N_ISP,K_share(TargetBS_id_share,:),phi_targetBS_share,User_id_targetBS_share,ISP_number_targetBS_share,frf,flag3,flag5,int_share_fss,int_share_esp,int_share_psp,int_share_ofts,UE_Locx(User_id_targetBS_share),UE_Locy(User_id_targetBS_share),BS_Locx(TargetBS_id_share),BS_Locy(TargetBS_id_share),Noise_db,theta_deg,ISP_flag,BS_den,UE_den_tot,percent,n_ofts,n_prs);
end

% BS target of ISP1 in not BS sharing, full spectrum sharing case
if sum(K_part(TargetBS_id_part_1,:))>0
    ISP_flag='1';  % non-BS sharing case for ISP1 (Spectrum sharing only)
    BS_Supervisor(ind_sim,MarketS,TargetBS_id_part_1,H_part_1,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE,P_transmit_tot,Noise,theta_min,OvheadLoss,Lossfact,ro_max,T_slot,N_ISP,K_part(TargetBS_id_part_1,:),phi_targetBS_part_1,User_id_targetBS_part_1,ISP_number_targetBS_part_1,frf,flag3,flag5,int_part_1_fss,int_part_1_esp,int_part_1_psp,int_part_1_ofts,UE_Locx(User_id_targetBS_part_1),UE_Locy(User_id_targetBS_part_1),BS_Locx(TargetBS_id_part_1),BS_Locy(TargetBS_id_part_1),Noise_db,theta_deg,ISP_flag,BS_den,UE_den_tot,percent,n_ofts,n_prs);
end

% BS target of ISP2 in not BS sharing, full spectrum sharing case
if sum(K_part(TargetBS_id_part_2,:))>0
    ISP_flag='2';  % non-BS sharing case for ISP2 (Spectrum sharing only)
    BS_Supervisor(ind_sim,MarketS,TargetBS_id_part_2,H_part_2,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE,P_transmit_tot,Noise,theta_min,OvheadLoss,Lossfact,ro_max,T_slot,N_ISP,K_part(TargetBS_id_part_2,:),phi_targetBS_part_2,User_id_targetBS_part_2,ISP_number_targetBS_part_2,frf,flag3,flag5,int_part_2_fss,int_part_2_esp,int_part_2_psp,int_part_2_ofts,UE_Locx(User_id_targetBS_part_2),UE_Locy(User_id_targetBS_part_2),BS_Locx(TargetBS_id_part_2),BS_Locy(TargetBS_id_part_2),Noise_db,theta_deg,ISP_flag,BS_den,UE_den_tot,percent,n_ofts,n_prs);
end

% BS target of ISP1 in not BS sharing, not spectrum sharing case
if sum(K_part(TargetBS_id_part_1,:))>0
    ISP_flag='-1';  % non-BS sharing case for ISP1 (No sharing)
    BS_Supervisor(ind_sim,MarketS,TargetBS_id_part_1,H_part_1,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE,P_transmit_tot,Noise,theta_min,OvheadLoss,Lossfact,ro_max,T_slot,N_ISP,K_part(TargetBS_id_part_1,:),phi_targetBS_part_1,User_id_targetBS_part_1,ISP_number_targetBS_part_1,frf,flag3,flag5,int_part_1_fss,int_part_1_esp,int_part_1_psp,int_part_1_ofts,UE_Locx(User_id_targetBS_part_1),UE_Locy(User_id_targetBS_part_1),BS_Locx(TargetBS_id_part_1),BS_Locy(TargetBS_id_part_1),Noise_db,theta_deg,ISP_flag,BS_den,UE_den_tot,percent,n_ofts,n_prs);
end


% BS target of ISP2 in not BS sharing, not spectrum sharing case
if sum(K_part(TargetBS_id_part_2,:))>0
    ISP_flag='-2';  % non-BS sharing case for ISP2 (No sharing)
    BS_Supervisor(ind_sim,MarketS,TargetBS_id_part_2,H_part_2,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE,P_transmit_tot,Noise,theta_min,OvheadLoss,Lossfact,ro_max,T_slot,N_ISP,K_part(TargetBS_id_part_2,:),phi_targetBS_part_2,User_id_targetBS_part_2,ISP_number_targetBS_part_2,frf,flag3,flag5,int_part_2_fss,int_part_2_esp,int_part_2_psp,int_part_2_ofts,UE_Locx(User_id_targetBS_part_2),UE_Locy(User_id_targetBS_part_2),BS_Locx(TargetBS_id_part_2),BS_Locy(TargetBS_id_part_2),Noise_db,theta_deg,ISP_flag,BS_den,UE_den_tot,percent,n_ofts,n_prs);
end

%% Saving Simulation parameters and layout

sim_filename = ['dat-sim/Sim_'     ...
    num2str(MarketS(1)) '_' ...
    num2str(MarketS(2)) '_' ...
    num2str(BS_den) '_'...
    num2str(UE_den_tot) '_'...
    num2str(theta_deg) '_'   ...
    num2str(Noise_db) '_'    ...
    flag3 '_' ...
    flag5 '_' ...
    num2str(n_ofts) '_' ...
    num2str(n_prs) '_' ...
    num2str(ind_sim) '.csv'];

fileID = fopen(sim_filename,'w');
formatSpec = 'Parameter,value\n';
fprintf(fileID,formatSpec);

formatSpec = 'Sim ID,%d\n';
fprintf(fileID,formatSpec,ind_sim);
formatSpec = 'flag1,%s\n';
fprintf(fileID,formatSpec,flag1);
formatSpec = 'flag2,%s\n';
fprintf(fileID,formatSpec,flag2);
formatSpec = 'flag3,%s\n';
fprintf(fileID,formatSpec,flag3);
formatSpec = 'flag5,%s\n';
fprintf(fileID,formatSpec,flag5);
formatSpec = 'UE_den (/km^2),%d\n';
fprintf(fileID,formatSpec,UE_den_tot);
formatSpec = 'BS_den (/km^2),%d\n';
fprintf(fileID,formatSpec,BS_den);
formatSpec = 'Num_Users1,%d\n';
fprintf(fileID,formatSpec,n(1));
formatSpec = 'Num_Users2,%d\n';
fprintf(fileID,formatSpec,n(2));
formatSpec = 'Num_BSs1,%d\n';
fprintf(fileID,formatSpec,N(1));
formatSpec = 'Num_BSs2,%d\n';
fprintf(fileID,formatSpec,N(2));
formatSpec = 'ISP_Market1,%f\n';
fprintf(fileID,formatSpec,MarketS(1));
ormatSpec = 'ISP_Market2,%f\n';
fprintf(fileID,formatSpec,MarketS(2));
formatSpec = 'P_transmit (W),%f\n';
fprintf(fileID,formatSpec,P_transmit_tot);
formatSpec = 'N_antenna_BS,%d\n';
fprintf(fileID,formatSpec,Na_BS);
formatSpec = 'N_antenna_UE,%d\n';
fprintf(fileID,formatSpec,Na_UE);
formatSpec = 'Side_lobe_BS,%f\n';
fprintf(fileID,formatSpec,side_lobe_BS);
formatSpec = 'Side_lobe_UE,%f\n';
fprintf(fileID,formatSpec,side_lobe_UE);
formatSpec = 'Sep_angle (deg),%f\n';
fprintf(fileID,formatSpec,theta_deg);
formatSpec = 'Beam_width_BS (deg),%f\n';
fprintf(fileID,formatSpec,beam_Ant_BS*(180/pi));
formatSpec = 'Beam_width_UE (deg),%f\n';
fprintf(fileID,formatSpec,beam_Ant_UE*(180/pi));
formatSpec = 'Noise_power (dB),%f\n';
fprintf(fileID,formatSpec,Noise_db);
formatSpec = 'Time_slot,%e\n';
fprintf(fileID,formatSpec,T_slot);
formatSpec = 'grid_accuracy (m),%f\n';
fprintf(fileID,formatSpec,d_grid);
formatSpec = 'Overhead_loss,%f\n';
fprintf(fileID,formatSpec,OvheadLoss);
formatSpec = 'SINR_loss,%f\n';
fprintf(fileID,formatSpec,Lossfact);
formatSpec = 'Rho_max,%f\n';
fprintf(fileID,formatSpec,ro_max);
formatSpec = 'N_OFTS,%f\n';
fprintf(fileID,formatSpec,n_ofts);
formatSpec = 'N_PRS,%f\n';
fprintf(fileID,formatSpec,n_prs);


fclose(fileID);


% Users Location
sim_filename = ['dat-lay/LayUE_'     ...
    num2str(MarketS(1)) '_' ...
    num2str(MarketS(2)) '_' ...
    num2str(BS_den) '_'...
    num2str(UE_den_tot) '_'...
    num2str(theta_deg) '_'   ...
    num2str(Noise_db) '_'    ...
    flag3 '_' ...
    flag5 '_' ...
    num2str(n_ofts) '_' ...
    num2str(n_prs) '_' ...
    num2str(ind_sim) '.csv'];

fileID = fopen(sim_filename,'w');
formatSpec = 'User ID,Parameter,Value\n';
fprintf(fileID,formatSpec);


for i=1:sum(n)
    formatSpec = '%f,locx,%d\n';
    fprintf(fileID,formatSpec,i,UE_Locx(i));
    formatSpec = '%f,locy,%d\n';
    fprintf(fileID,formatSpec,i,UE_Locy(i));
    formatSpec = '%f,ispNumber,%d\n';
    fprintf(fileID,formatSpec,i,UE_ISP(i));
end
fclose(fileID);


% BSs Location
sim_filename = ['dat-lay/LayBS_'     ...
    num2str(MarketS(1)) '_' ...
    num2str(MarketS(2)) '_' ...
    num2str(BS_den) '_'...
    num2str(UE_den_tot) '_'...
    num2str(theta_deg) '_'   ...
    num2str(Noise_db) '_'    ...
    flag3 '_' ...
    flag5 '_' ...
    num2str(n_ofts) '_' ...
    num2str(n_prs) '_' ...
    num2str(ind_sim) '.csv'];

fileID = fopen(sim_filename,'w');
formatSpec = 'BS ID,Parameter,Value\n';
fprintf(fileID,formatSpec);


for i=1:sum(N)
    formatSpec = '%f,locx,%d\n';
    fprintf(fileID,formatSpec,i,BS_Locx(i));
    formatSpec = '%f,locy,%d\n';
    fprintf(fileID,formatSpec,i,BS_Locy(i));
    formatSpec = '%f,ispNumber,%d\n';
    fprintf(fileID,formatSpec,i,find(BS_ISP_part(i,:)==1));
end
fclose(fileID);


% UE-BS Association
sim_filename = ['dat-lay/LayAsso_'     ...
    num2str(MarketS(1)) '_' ...
    num2str(MarketS(2)) '_' ...
    num2str(BS_den) '_'...
    num2str(UE_den_tot) '_'...
    num2str(theta_deg) '_'   ...
    num2str(Noise_db) '_'    ...
    flag3 '_' ...
    flag5 '_' ...
    num2str(n_ofts) '_' ...
    num2str(n_prs) '_' ...
    num2str(ind_sim) '.csv'];

fileID = fopen(sim_filename,'w');
formatSpec = 'User ID,Scenario,BS ID\n';
fprintf(fileID,formatSpec);


for i=1:sum(n)
    formatSpec = '%f,bsShare,%d\n';
    fprintf(fileID,formatSpec,i,find(UE_BS_share(i,:)==1));
    formatSpec = '%f,noBsShare,%d\n';
    fprintf(fileID,formatSpec,i,find(UE_BS_part(i,:)==1));
end
fclose(fileID);



end
