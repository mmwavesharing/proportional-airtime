function [User_SNR_All,User_INR_All,User_IINR_All,User_OINR_All,User_SINR_All,User_SINR,User_Rate,User_Rate_SNR,User_Rate_SIINR,User_Rate_SOINR,ISP_Rate,User_per_slot,User_Share]=Proportional_Resource_Sharing(IntfcO,H,phi,T_slot,theta_min,N_ISP,ISP_number,P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE,percent,n_prs)

N_user=length(ISP_number);
N1=sum(ISP_number==1);
N2=sum(ISP_number==2);
b=zeros([N_user,1]);              % Buckets, a credit vector for users to establish the temporal fairness
p=(1-percent)/percent;
%w=(1/(N1+N2*p))*(ISP_number==1)+(p/(N1+N2*p))*(ISP_number==2); % Scheduling weights of the users

if N1==0
    w=ones(N2,1)./N2
elseif N2==0
    w=ones(N1,1)./N1
else
    w=(n_prs/N1)*(ISP_number==1)+((1-n_prs)/N2)*(ISP_number==2);
end

User_Rate=zeros(N_user,1);    % Users' average rate vector
User_Rate_SNR=zeros(N_user,1);
User_Rate_SIINR=zeros(N_user,1);
User_Rate_SOINR=zeros(N_user,1);
User_SINR=zeros(N_user,1);     % Users' average SINR vector when they are scheduled
User_SNR_All=zeros(N_user,1); % Users' average SINR vector over all time slots
User_SINR_All=zeros(N_user,1); % Users' average SINR vector over all time slots
User_INR_All=zeros(N_user,1); % Users' average INR vector over all time slots
User_IINR_All=zeros(N_user,1); % Users' average intra-cell INR vector over all time slots
User_OINR_All=zeros(N_user,1); % Users' average inter-cell INR vector over all time slots
Counter=zeros([N_user,1]);      % Counter vector for users to measure the fairness
Counter_ISP=zeros([N_ISP,1]);      % Average number of scheduled users per time slots for ISPs

NUmax=floor((2*pi)/theta_min);
P_transmit=P_transmit_tot/NUmax;



for i=1:1:T_slot
    
    IntInt=(side_lobe_BS/NUmax)*Na_UE*(NUmax-1)*H(:,i)*P_transmit;  % Amount of intra-cell interference assuming maximum number of users are active
    R=min((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit*H(:,i))./(Noise+IntInt)))),ro_max); % Users' rate in the current time slot
    
    % scheduling
    User_index=zeros(N_user,1);    % Auxilliary vector in scheduling, we are done in each time slot if all of the elements of it become 1
    j=1;
    k=1;
    U=R+(alpha*b);                 % Algorithm A : Opportunistic Fair Scheduling
    [A,C(j)]=max(U);
    Ch(k)=C(j);
    User_index(C(j))=1;
    Th(j)=phi(C(j));
    R(C(j))=-inf;                  % Eliminating the selected user from the list for the current time slot because it cannot be selected more than once
    Counter(C(j))=Counter(C(j))+1; % updating the counter of the selected user
    Counter_ISP(ISP_number(C(j)))=Counter_ISP(ISP_number(C(j)))+1;
    b(C(j))=b(C(j))-1;             % updating the bucket(credit) of the selected user
    b=b+w;                         % updating the bucket(credit) of all the users based on the scheduling weights
    j=j+1;
    k=k+1;
    U=R+(alpha*b);                 % Recalculate the objective function
    
    while  sum(User_index)<N_user
        
        %           if j>(pi/theta_min)      %Algorithm B : Pure Opportunistic Scheduling
        %               [A,C(j)]=max(R);
        %               User_index(C(j))=1;
        %               R(C(j))=-inf;
        %               cond=0;
        %               if (min(abs(phi(C(j))-Th))<theta_min) || (min((2*pi)-abs(phi(C(j))-Th))<theta_min)
        %                   cond=1;
        %               end
        %
        %               if cond==0
        %                   Ch(k)=C(j);
        %                   Counter(C(j))=Counter(C(j))+1;
        %                   Counter_op(ISP_number(C(j)))=Counter_op(ISP_number(C(j)))+1;
        %                   %b(C(j))=b(C(j))-1;
        %                   %b=b+w;
        %                   %U=R+(alpha*b);
        %                   Th(j)=phi(C(j));
        %                   j=j+1;
        %                   k=k+1;
        %               end
        %
        %           else
        
        [A,C(j)]=max(U);
        User_index(C(j))=1;
        R(C(j))=-inf;
        cond=0;
        if (min(abs(phi(C(j))-Th))<theta_min) || (min((2*pi)-abs(phi(C(j))-Th))<theta_min)
            cond=1;
        end
        
        if cond==0
            Ch(k)=C(j);
            Counter(C(j))=Counter(C(j))+1;
            Counter_ISP(ISP_number(C(j)))=Counter_ISP(ISP_number(C(j)))+1;
            b(C(j))=b(C(j))-1;
            b=b+w;
            U=R+(alpha*b);
            Th(j)=phi(C(j));
            j=j+1;
            k=k+1;
        else
            U(C(j))=-inf;
        end
        
        %end
        
    end
    
    Nau=length(Ch);                                              % Number of active (scheduled) users (# of streams)
    BF= Na_UE*max(floor(Na_BS/Nau),1);                                    % Maximum beamforming gain in linear scale
    IntfcI=(side_lobe_BS/Nau)*Na_UE*(Nau-1)*H(:,i)*(P_transmit_tot/Nau);  % Inter-cell interference
    User_Rate(Ch)=User_Rate(Ch)+min((1-OvheadLoss)*log2(1+(Lossfact*((((P_transmit_tot*BF)/Nau)*H(Ch,i))./(Noise+IntfcO(Ch,i)+IntfcI(Ch))))),ro_max);
    User_SINR(Ch)=User_SINR(Ch)+((((P_transmit_tot*BF)/Nau)*H(Ch,i))./(Noise+IntfcO(Ch,i)+IntfcI(Ch)));
    
    User_Rate_SNR(Ch)=User_Rate_SNR(Ch)+min((1-OvheadLoss)*log2(1+(Lossfact*((((P_transmit_tot*BF)/Nau)*H(Ch,i))./(Noise)))),ro_max);
    User_Rate_SIINR(Ch)=User_Rate_SIINR(Ch)+min((1-OvheadLoss)*log2(1+(Lossfact*((((P_transmit_tot*BF)/Nau)*H(Ch,i))./(Noise+IntfcI(Ch))))),ro_max);
    User_Rate_SOINR(Ch)=User_Rate_SOINR(Ch)+min((1-OvheadLoss)*log2(1+(Lossfact*((((P_transmit_tot*BF)/Nau)*H(Ch,i))./(Noise+IntfcO(Ch,i))))),ro_max);
    
    
    BF_All= Na_UE*max(floor(Na_BS/NUmax),1);                              % Beamforming gain when the maximum number of the users are scheduled
    P_transmit_All=P_transmit_tot./NUmax;                 % BS TX power to each user when the maximum number of the users are scheduled
    User_SNR_All=User_SNR_All+((BF_All*P_transmit_All*H(:,i))./Noise);
    User_SINR_All=User_SINR_All+((BF_All*P_transmit_All*H(:,i))./(Noise+IntfcO(:,i)+IntInt));
    User_INR_All=User_INR_All+((IntfcO(:,i)+IntInt)./Noise);
    User_IINR_All=User_IINR_All+(IntInt./Noise);
    User_OINR_All=User_OINR_All+(IntfcO(:,i)./Noise);
    
    Th=[];
    C=[];
    Ch=[];
    j=1;
    k=1;
    
    
end

%% Reseting the parameters for the next iteration
User_per_slot=Counter_ISP/T_slot;
User_Share=Counter./T_slot;
User_Rate=User_Rate./T_slot;

User_Rate_SNR=User_Rate_SNR./T_slot;
User_Rate_SIINR=User_Rate_SIINR./T_slot;
User_Rate_SOINR=User_Rate_SOINR./T_slot;


User_SINR=(User_SINR./T_slot)./User_Share;
User_SINR_All=User_SINR_All./T_slot;
User_SNR_All=User_SNR_All./T_slot;
User_INR_All=User_INR_All./T_slot;
User_IINR_All=User_IINR_All./T_slot;
User_OINR_All=User_OINR_All./T_slot;
ISP_Rate=[sum(User_Rate(ISP_number==1));sum(User_Rate(ISP_number==2))];



end
