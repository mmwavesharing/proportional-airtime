function[User_INR_All,User_SNR_All,User_SNR,User_Rate,Rate_ISP,User_per_slot,User_Share,Counter_ISP]=Partial_Spectrum_Sharing(Intfc,H,phi,T_slot,theta_min,Sharing_weight,N_user_ISP,user_vector1,user_vector2,P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise_net,alpha,Na,side_lobe)
                                                                                                    
% Scheduling parameters
b1=zeros([N_user_ISP(1),1]); % Bucket vector of users of ISP 1
b2=zeros([N_user_ISP(2),1]); % Bucket vector of users of ISP 2
w1=ones(1,N_user_ISP(1))'./N_user_ISP(1); % Scheduling weights of the users of ISP 1
w2=ones(1,N_user_ISP(2))'./N_user_ISP(2); % Scheduling weights of the users of ISP 2
Counter1=zeros([N_user_ISP(1),1]); % Counter vector for users of ISP 1 to measure the fairness
Counter2=zeros([N_user_ISP(2),1]); % Counter vector for users of ISP 2 to measure the fairness
User_Rate1=zeros(N_user_ISP(1),1); % Rate vector of users of ISP 1
User_Rate2=zeros(N_user_ISP(2),1); % Rate vector of users of ISP 2
User_SNR1=zeros(N_user_ISP(1),1);  % Average SNR vector of users of ISP 1 when they are scheduled
User_SNR2=zeros(N_user_ISP(2),1);  % Average SNR vector of users of ISP 2 when they are scheduled
User_SNR1_All=zeros(N_user_ISP(1),1);  % Average SNR vector of users of ISP 1 
User_SNR2_All=zeros(N_user_ISP(2),1);  % Average SNR vector of users of ISP 2
Counter_ISP=zeros(2,1);
Rate_ISP=zeros(2,1);  % Total rate of ISPs
Noise=Noise_net.*[(1+Sharing_weight)/2,(1-Sharing_weight)/2]; % Noise power vector
P_transmit=P_transmit_tot/(2*floor((2*pi)/theta_min));

for i=1:1:T_slot
    
    % Operator 1 scheduling
    H1=H(user_vector1,i);
    R1=min((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit*H1)/Noise(1)))),ro_max);
    phi1=phi(user_vector1);
    
    User_index1=zeros(N_user_ISP(1),1); % Auxilliary vector in scheduling, we are done in each time slot if all of the elements of it become 1
    j1=1; % Auxiliary counter
    k1=1; % Auxiliary counter
    U1=R1+(alpha*b1); %Algorithm A : Opportunistic Fair Scheduling
    [A1,C1(j1)]=max(U1);
    Ch1(k1)=C1(j1);
    User_index1(C1(j1))=1;
    Th1(j1)=phi1(C1(j1));
    R1(C1(j1))=-inf; % Eliminating the selected user from the list for the current time slot because it cannot be selected more than once
    b1(C1(j1))=b1(C1(j1))-1; % Updating the bucket(credit) of the selected user
    Counter1(C1(j1))=Counter1(C1(j1))+1; % updating the counter of the selected user
    b1=b1+w1; % updating the bucket(credit) of all the users based on the scheduling weights
    j1=j1+1;
    k1=k1+1;
    U1=R1+(alpha*b1); % Recalculate the objective function
    
    while  sum(User_index1)<N_user_ISP(1)
        
%         if j1>(pi/theta_min)  %Algorithm B : Pure Opportunistic Scheduling
%             [A1,C1(j1)]=max(R1);
%             User_index1(C1(j1))=1;
%             R1(C1(j1))=-inf;
%             cond=0;
%             if (min(abs(phi1(C1(j1))-Th1))<theta_min) || (min((2*pi)-abs(phi1(C1(j1))-Th1))<theta_min)
%                 cond=1;
%             end
%             
%             if cond==0
%                 Ch1(k1)=C1(j1);
%                 Counter1(C1(j1))=Counter1(C1(j1))+1;
%                 Th1(j1)=phi1(C1(j1));
%                 j1=j1+1;
%                 k1=k1+1;
%             end
%             
%         else
            
            [A1,C1(j1)]=max(U1);
            User_index1(C1(j1))=1;
            R1(C1(j1))=-inf;
            cond=0;
            if (min(abs(phi1(C1(j1))-Th1))<theta_min) || (min((2*pi)-abs(phi1(C1(j1))-Th1))<theta_min)
                cond=1;
            end
            
            if cond==0
                Ch1(k1)=C1(j1);
                Counter1(C1(j1))=Counter1(C1(j1))+1;
                b1(C1(j1))=b1(C1(j1))-1;
                b1=b1+w1;
                U1=R1+(alpha*b1);
                Th1(j1)=phi1(C1(j1));
                j1=j1+1;
                k1=k1+1;
            else
                U1(C1(j1))=-inf;
            end
            
        %end
        
    end
    %Th/(2*pi)*360
   
    
    
    % Operator 2 scheduling
    H2=H(user_vector2,i);
    R2=min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit*H2)/Noise(1))))),ro_max);
    phi2=phi(user_vector2);
    
    User_index2=zeros(N_user_ISP(2),1); % Auxilliary vector in scheduling, we are done in each time slot if all of the elements of it become 1
    j2=1;
    k2=1;
    U2=R2+(alpha*b2); %Algorithm A : Opportunistic Fair Scheduling
    [A2,C2(j2)]=max(U2);
    Ch2(k2)=C2(j2);
    User_index2(C2(j2))=1;
    Th2(j2)=phi2(C2(j2));
    R2(C2(j2))=-inf; % Eliminating the selected user from the list for the current time slot because it cannot be selected more than once
    b2(C2(j2))=b2(C2(j2))-1; % Updating the bucket(credit) of the selected user
    Counter2(C2(j2))=Counter2(C2(j2))+1; % updating the counter of the selected user
    b2=b2+w2; % updating the bucket(credit) of all the users based on the scheduling weights
    j2=j2+1;
    k2=k2+1;
    U2=R2+(alpha*b2); % Recalculate the objective function
    
    while  sum(User_index2)<N_user_ISP(2)
        
%         if j2>(pi/theta_min)  %Algorithm B : Pure Opportunistic Scheduling
%             [A2,C2(j2)]=max(R2);
%             User_index2(C2(j2))=1;
%             R2(C2(j2))=-inf;
%             cond=0;
%             if (min(abs(phi2(C2(j2))-Th2))<theta_min) || (min((2*pi)-abs(phi2(C2(j2))-Th2))<theta_min)
%                 cond=1;
%             end
%             
%             if cond==0
%                 Ch2(k2)=C2(j2); 
%                 Counter2(C2(j2))=Counter2(C2(j2))+1;
%                 Th2(j2)=phi2(C2(j2));
%                 j2=j2+1;
%                 k2=k2+1;
%             end
%             
%         else
            
            [A2,C2(j2)]=max(U2);
            User_index2(C2(j2))=1;
            R2(C2(j2))=-inf;
            cond=0;
            if (min(abs(phi2(C2(j2))-Th2))<theta_min) || (min((2*pi)-abs(phi2(C2(j2))-Th2))<theta_min)
                cond=1;
            end
            
            if cond==0
                Ch2(k2)=C2(j2);
                Counter2(C2(j2))=Counter2(C2(j2))+1;
                b2(C2(j2))=b2(C2(j2))-1;
                b2=b2+w2;
                U2=R2+(alpha*b2);
                Th2(j2)=phi2(C2(j2));
                j2=j2+1;
                k2=k2+1;
            else
                U2(C2(j2))=-inf;
            end
            
        %end
        
    end
    %Th/(2*pi)*360
    
    Ns1=length(Ch1);                  % Number of scheduled users of ISP1
    Ns2=length(Ch2);                  % Number of scheduled users of ISP1
    BF= max(floor(Na/(Ns1+Ns2)),1);   % Beamforming gain
    NsMax=2*floor((2*pi)/theta_min);  % Maximum number of the users which can be scheduled at a time slot
    BF_All= max(floor(Na/NsMax),1);   % Beamforming gain when the maximum number of the users are scheduled
    
 % First case : ISP1 gets the larger part
   P_transmit11=(((1+Sharing_weight)/2)*P_transmit_tot)*BF./Ns1;
   P_transmit12=(((1-Sharing_weight)/2)*P_transmit_tot)*BF./Ns2;
   Rate11=((1+Sharing_weight)/2)*min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit11.*H1(Ch1))/Noise(1))))),ro_max);
   Rate12=((1-Sharing_weight)/2)*min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit12.*H2(Ch2))/Noise(2))))),ro_max);
   
   P_transmit11_All=(((1+Sharing_weight)/2)*P_transmit_tot)*BF_All./(NsMax/2);
   P_transmit12_All=(((1-Sharing_weight)/2)*P_transmit_tot)*BF_All./(NsMax/2);
  
   % Second case : ISP2 gets the larger part
   P_transmit21=(((1-Sharing_weight)/2)*P_transmit_tot)*BF./Ns1;
   P_transmit22=(((1+Sharing_weight)/2)*P_transmit_tot)*BF./Ns2;
   Rate21=((1-Sharing_weight)/2)*min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit21.*H1(Ch1))/Noise(2))))),ro_max);
   Rate22=((1+Sharing_weight)/2)*min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit22.*H2(Ch2))/Noise(1))))),ro_max);

   P_transmit21_All=(((1-Sharing_weight)/2)*P_transmit_tot)*BF_All./(NsMax/2);
   P_transmit22_All=(((1+Sharing_weight)/2)*P_transmit_tot)*BF_All./(NsMax/2);
   
   
   
   
   % Choosing one ISP to use the larger part of the spectrum (Fully Opportunistic selection)
    if (sum(Rate11)+sum(Rate12)) > (sum(Rate21)+sum(Rate22)) % ISP1 is chosen
        Counter_ISP(1)=Counter_ISP(1)+1; 
        Rate_ISP(1)=Rate_ISP(1)+sum(Rate11);
        Rate_ISP(2)=Rate_ISP(2)+sum(Rate12);   
        User_Rate1(Ch1)=User_Rate1(Ch1)+Rate11;
        User_Rate2(Ch2)=User_Rate2(Ch2)+Rate12;
        User_SNR1(Ch1)=User_SNR1(Ch1)+((P_transmit11*H1(Ch1))/Noise(1));
        User_SNR2(Ch2)=User_SNR2(Ch2)+((P_transmit12*H2(Ch2))/Noise(2)); 
        User_SNR1_All=User_SNR1_All+((P_transmit11_All*H1)/Noise(1));
        User_SNR2_All=User_SNR2_All+((P_transmit12_All*H2)/Noise(2));
    else % ISP2 is chosen
        Counter_ISP(2)=Counter_ISP(2)+1;
        Rate_ISP(1)=Rate_ISP(1)+sum(Rate21);
        Rate_ISP(2)=Rate_ISP(2)+sum(Rate22); 
        User_Rate1(Ch1)=User_Rate1(Ch1)+Rate21;
        User_Rate2(Ch2)=User_Rate2(Ch2)+Rate22;
        User_SNR1(Ch1)=User_SNR1(Ch1)+((P_transmit21*H1(Ch1))/Noise(2));
        User_SNR2(Ch2)=User_SNR2(Ch2)+((P_transmit22*H2(Ch2))/Noise(1));
        User_SNR1_All=User_SNR1_All+((P_transmit21_All*H1)/Noise(2));
        User_SNR2_All=User_SNR2_All+((P_transmit22_All*H2)/Noise(1));
    end
    
 
%     if length(Ch2)-length(C2(1:end-1))~=0
%         Ch2
%         C2(1:end-1)
%         360*Th2/(2*pi)
%     end
%     if length(Ch1)-length(C1(1:end-1))~=0
%         Ch1
%         C1(1:end-1)
%         360*Th1/(2*pi)
%     end
    
%     length(Ch1)-length(Th1)
%     length(Ch2)-length(Th2)
    
    Th1=[];
    C1=[];
    Ch1=[];
    k1=1;
    j1=1;
    Th2=[];
    C2=[];
    Ch2=[];
    k2=1;
    j2=1;
   
end

User_Share=[Counter1;Counter2]/T_slot;
User_per_slot=[sum(Counter1);sum(Counter2)]./T_slot;
Counter_ISP=Counter_ISP./T_slot;
Rate_ISP=Rate_ISP./T_slot;
User_Rate=[User_Rate1;User_Rate2]./T_slot;
User_SNR=[User_SNR1;User_SNR2]./T_slot;
User_SNR_All=[User_SNR1_All;User_SNR2_All]./T_slot;
end