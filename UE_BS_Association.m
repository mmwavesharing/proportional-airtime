function [TargetBS_id_share,TargetBS_id_part_1,TargetBS_id_part_2,TargetBSx_share,TargetBSx_part_1,TargetBSx_part_2,TargetBSy_share,TargetBSy_part_1,TargetBSy_part_2,phi_targetBS_share,phi_targetBS_part_1,phi_targetBS_part_2,ISP_number_targetBS_share,ISP_number_targetBS_part_1,ISP_number_targetBS_part_2,User_id_targetBS_share,User_id_targetBS_part_1,User_id_targetBS_part_2,N_user_targetBS_share,N_user_targetBS_part_1,N_user_targetBS_part_2,d_ue_bs,UE_BS_share,UE_BS_part,D_UE_BS_share,D_UE_BS_part,Phi_UE_BS_share,Phi_UE_BS_part,K_share,K_part,ue_isp_bs_share,ue_isp_bs_part]=UE_BS_Association(n,N,UE_ISP,BS_Locx,BS_Locy,UE_Locx,UE_Locy,BS_ISP_share,BS_ISP_part)
% BS partitioning case
UE_BS_part=zeros(sum(n),sum(N));       % UE_BS association matrix
D_UE_BS_part=zeros(sum(n),sum(N));     % UE_BS distance matrix
Phi_UE_BS_part=zeros(sum(n),sum(N));   % UE_BS relative angular position matrix
K_part=zeros(sum(N),2);                % Number of users of the BSs
m_part=ones(sum(N),1);                 % Counter
ue_isp_bs_part=cell(sum(N),1);         % ISP of the users served by each BS


% BS sharing case
UE_BS_share=zeros(sum(n),sum(N));      % UE_BS association matrix
D_UE_BS_share=zeros(sum(n),sum(N));    % UE_BS distance matrix
Phi_UE_BS_share=zeros(sum(n),sum(N));  % UE_BS relative angular position matrix
K_share=zeros(sum(N),2);               % Number of users of the BSs
m_share=ones(sum(N),1);                % Counter
ue_isp_bs_share=cell(sum(N),1);        % ISP of the users served by each BS

for i=1:1:sum(n)
    V_share=BS_ISP_share(:,UE_ISP(i)); % Finding available BSs to serve the user
    V_part=BS_ISP_part(:,UE_ISP(i));
    d_ue_bs(i,:)=sqrt((UE_Locx(i)-BS_Locx).^2+(UE_Locy(i)-BS_Locy).^2);                             % Distance of each user to each BS
    [X_share,U_share]=min((UE_Locx(i)-BS_Locx(V_share==1)).^2+(UE_Locy(i)-BS_Locy(V_share==1)).^2); % Finding the nearest available BS to this UE
    [X_part,U_part]=  min((UE_Locx(i)-BS_Locx(V_part==1)).^2+(UE_Locy(i)-BS_Locy(V_part==1)).^2);
    UE_BS_share(i,U_share)=1;          % Association
    A=N(1)*(UE_ISP(i)==2);
    UE_BS_part(i,U_part+A)=1;
    theta_share=atan2(UE_Locy(i)-BS_Locy(U_share),UE_Locx(i)-BS_Locx(U_share));                     % Angular position of the UE relative to the selected BS [-pi pi]
    theta_part=atan2(UE_Locy(i)-BS_Locy(U_part+A),UE_Locx(i)-BS_Locx(U_part+A));
    
    Phi_UE_BS_share(i,U_share)=(theta_share>=0)*theta_share+(theta_share<0)*(theta_share+2*pi);     % Transform theta to be in [0 2*pi]
    Phi_UE_BS_part(i,U_part+A)=(theta_part>=0)*theta_part+(theta_part<0)*(theta_part+2*pi);
    
    D_UE_BS_share(i,U_share)=sqrt(X_share);                                                         % Distance of the UE to the selected BS
    D_UE_BS_part(i,U_part+A)=sqrt(X_part);
    
    K_share(U_share,UE_ISP(i))=K_share(U_share,UE_ISP(i))+1;                                        % Number of the UEs of each ISP served by each BS
    K_part(U_part+A,UE_ISP(i))=K_part(U_part+A,UE_ISP(i))+1;
    
    ue_isp_bs_share{U_share}(m_share(U_share))=UE_ISP(i);                                           % ISP of the users served by each BS
    ue_isp_bs_part{U_part+A}(m_part(U_part+A))=UE_ISP(i);
    
    m_share(U_share)=m_share(U_share)+1;
    m_part(U_part+A)=m_part(U_part+A)+1;
end

%% Fiding target BSs

% Wihout BS sharing ISP1
[~,B]=sort(BS_Locx(1:N(1)).^2+BS_Locy(1:N(1)).^2);
checkbox=0;
counter=1;
while checkbox==0
    if K_part(B(counter),1)>=1
        checkbox=1;
        TargetBS_id_part_1=B(counter);
        TargetBSx_part_1=BS_Locx(B(counter));
        TargetBSy_part_1=BS_Locy(B(counter));
    end
    counter=counter+1;
end

% Wihout BS sharing ISP2
[~,B]=sort(BS_Locx(N(1)+1:sum(N)).^2+BS_Locy(N(1)+1:sum(N)).^2);
checkbox=0;
counter=1;
while checkbox==0
    if K_part(B(counter)+N(1),2)>=1
        checkbox=1;
        TargetBS_id_part_2=B(counter)+N(1);
        TargetBSx_part_2=BS_Locx(B(counter)+N(1));
        TargetBSy_part_2=BS_Locy(B(counter)+N(1));
    end
    counter=counter+1;
end

% BS sharing case
 
% [~,B]=sort(BS_Locx.^2+BS_Locy.^2);
% checkbox=0;
% counter=1;
% while checkbox==0
%     if (K_share(B(counter),1)>=1) && (K_share(B(counter),2)>=1)
%         checkbox=1;
%         TargetBS_id_share=B(counter);
%         TargetBSx_share=BS_Locx(B(counter));
%         TargetBSy_share=BS_Locy(B(counter));
%     end
%     counter=counter+1;
% end


% Wihout BS sharing ISP2
[~,B]=sort(BS_Locx.^2+BS_Locy.^2);
checkbox=0;
counter=1;
while checkbox==0
    if sum(K_share(B(counter),:))>=1
        checkbox=1;
        TargetBS_id_share=B(counter);
        TargetBSx_share=BS_Locx(B(counter));
        TargetBSy_share=BS_Locy(B(counter));
    end
    counter=counter+1;
end



ISP_number_targetBS_share=ue_isp_bs_share{TargetBS_id_share}(:);
User_id_targetBS_share=find(UE_BS_share(:,TargetBS_id_share)==1);
N_user_targetBS_share=length(User_id_targetBS_share);
phi_targetBS_share=Phi_UE_BS_share(User_id_targetBS_share,TargetBS_id_share);

ISP_number_targetBS_part_1=ue_isp_bs_part{TargetBS_id_part_1}(:);
User_id_targetBS_part_1=find(UE_BS_part(:,TargetBS_id_part_1)==1);
N_user_targetBS_part_1=length(User_id_targetBS_part_1);
phi_targetBS_part_1=Phi_UE_BS_part(User_id_targetBS_part_1,TargetBS_id_part_1);

ISP_number_targetBS_part_2=ue_isp_bs_part{TargetBS_id_part_2}(:);
User_id_targetBS_part_2=find(UE_BS_part(:,TargetBS_id_part_2)==1);
N_user_targetBS_part_2=length(User_id_targetBS_part_2);
phi_targetBS_part_2=Phi_UE_BS_part(User_id_targetBS_part_2,TargetBS_id_part_2);


end
