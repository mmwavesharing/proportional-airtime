function [n,UE_Locx,UE_Locy,UE_ISP]=User_Drop(flag2,N_ISP,N,N_UE,Sim_win,BS_Locx,BS_Locy,d_grid,r_grid)

n=zeros(N_ISP,1);                % Number of users of ISPs in this drop based on hPPP model
while n(1)==0 || n(2)==0
    n(1)=random('poisson',N_UE(1));
    n(2)=random('poisson',N_UE(2));
end

UE_Locx=zeros(sum(n),1);
UE_Locy=zeros(sum(n),1);


if strcmp(flag2,'1') % With spatial correlation
    
    % Creating a grid for the network
    x_g_min=(min(BS_Locx)>-Sim_win/2)*(-Sim_win/2)+(min(BS_Locx)<=-Sim_win/2)*(min(BS_Locx)-Sim_win/20);
    x_g_max=(max(BS_Locx)< Sim_win/2)*(Sim_win/2)+(max(BS_Locx)>= Sim_win/2)*(max(BS_Locx)+Sim_win/20);
    y_g_min=(min(BS_Locy)>-Sim_win/2)*(-Sim_win/2)+(min(BS_Locy)<=-Sim_win/2)*(min(BS_Locy)-Sim_win/20);
    y_g_max=(max(BS_Locy)< Sim_win/2)*(Sim_win/2)+(max(BS_Locy)>= Sim_win/2)*(max(BS_Locy)+Sim_win/20);
    
    x_g=[x_g_min:d_grid:x_g_max];
    y_g=[y_g_min:d_grid:y_g_max];
    [X_grid,Y_grid]=meshgrid(x_g,y_g);
    X_grid=X_grid(:);                  % x vector of the grid points
    Y_grid=Y_grid(:);                  % y vector of the grid points
    
    
    % ISP1
    k1=ones(N(1),1);
    for i=1:length(X_grid)
        [L_grid1,U_grid1]=min((X_grid(i)-BS_Locx(1:N(1))).^2+(Y_grid(i)-BS_Locy(1:N(1))).^2);  % Finding the nearest available BS of ISP1 to this grid point
        Gr1(U_grid1,k1(U_grid1))=i;
        k1(U_grid1)=k1(U_grid1)+1;
    end
    k1=k1-1;
    
    % Calculating number of users in each voronoi cell of ISP1 (uniformly at random)
    NUE_V1=zeros(N(1),1);               % Vector of number of the users of each BS
    rni=randi(N(1),[n(1),1]);
    for j=1:N(1)
        NUE_V1(j)=sum(rni==j);
    end
    
    %     %test
    %     hold on
    %     plot(X_grid(Gr1(4,1:k1(4))),Y_grid(Gr1(4,1:k1(4))),'.r')
    
    
    % User deployment
    kk=1; % Auxiliary counter
    for i=1:N(1)
        UEV=zeros(NUE_V1(i),1);
        rni=randi(k1(i),[NUE_V1(i),1]);
        r=r_grid.*sqrt(rand(NUE_V1(i),1));
        ph=2*pi.*rand(NUE_V1(i),1);
        UE_Locx(kk:kk+NUE_V1(i)-1)=X_grid(Gr1(i,rni))+r.*cos(ph);
        UE_Locy(kk:kk+NUE_V1(i)-1)=Y_grid(Gr1(i,rni))+r.*sin(ph);
        kk=kk+NUE_V1(i);
    end
    
    
    
    % ISP2
    k2=ones(N(2),1);
    for i=1:length(X_grid)
        [L_grid2,U_grid2]=min((X_grid(i)-BS_Locx(N(1)+1:sum(N))).^2+(Y_grid(i)-BS_Locy(N(1)+1:sum(N))).^2); % Finding the nearest available BS of ISP2 to this grid point
        Gr2(U_grid2,k2(U_grid2))=i;
        k2(U_grid2)=k2(U_grid2)+1;
    end
    k2=k2-1;
    
    % Calculating number of users in each voronoi cell of ISP2 (uniformly at random)
    NUE_V2=zeros(N(2),1);               % Vector of number of the users of each BS
    rni=randi(N(2),[n(2),1]);
    for j=1:N(2)
        NUE_V2(j)=sum(rni==j);
    end
    
    kk=n(1)+1; % Auxiliary counter
    
    for i=1:N(2)
        UEV=zeros(NUE_V2(i),1);
        rni=randi(k2(i),[NUE_V2(i),1]);
        r=r_grid.*rand(NUE_V2(i),1);
        ph=2*pi.*rand(NUE_V2(i),1);
        UE_Locx(kk:kk+NUE_V2(i)-1)=X_grid(Gr2(i,rni))+r.*cos(ph);
        UE_Locy(kk:kk+NUE_V2(i)-1)=Y_grid(Gr2(i,rni))+r.*sin(ph);
        kk=kk+NUE_V2(i);
    end
    
    UE_ISP=zeros(sum(n),1);
    UE_ISP(1:n(1))=ones(n(1),1);
    UE_ISP(n(1)+1:sum(n))=2*ones(n(2),1);
    
elseif strcmp(flag2,'0') % without spatial correlation
    
    UE_Locx=zeros(sum(n),1);
    UE_Locy=zeros(sum(n),1);
    UE_Locx(1:n(1))=-(Sim_win/2)+Sim_win*rand([n(1),1]);
    UE_Locy(1:n(1))=-(Sim_win/2)+Sim_win*rand([n(1),1]);
    UE_Locx(n(1)+1:sum(n))=-(Sim_win/2)+Sim_win*rand([n(2),1]);
    UE_Locy(n(1)+1:sum(n))=-(Sim_win/2)+Sim_win*rand([n(2),1]);
    
    %        % Test
    %
    %         hold on
    %         plot(UE_Locx(1:n(1)),UE_Locy(1:n(1)),'ok','LineWidth',2);
    %         hold on
    %         plot(UE_Locx(n(1)+1:sum(n)),UE_Locy(n(1)+1:sum(n)),'or','LineWidth',2)
    %
    UE_ISP=zeros(sum(n),1);
    UE_ISP(1:n(1))=ones(n(1),1);
    UE_ISP(n(1)+1:n(1)+n(2))=2*ones(n(2),1);
    
else
    disp('Error occured in user deployment procedure, UE deployment mode is not available')
    
end

end
