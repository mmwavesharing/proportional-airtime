function [Int_FSS,Int_ESP,Int_PSP,Int_OFTS]=Interference_Calculator(User_id,N,MarketS,UE_Locx,UE_Locy,BS_Locx,BS_Locy,Phi_UE_BS,K,UE_BS,UE_ISP,beam_Ant_BS,beam_Ant_UE,side_lobe_BS,side_lobe_UE,theta_deg,P_transmit_tot,flag,Na_BS,Na_UE,T_slot,d_inf,PrLOS,PrOUT,plLOS,plNLOS,sigma_LOS,sigma_NLOS,Threshold_int,label_sp,label_ss,BStarget,n_ofts,n_prs)

%This function calculates the interference for a target user with the identification User_id
%Channel between the user and the interferer base stations

BSx=BS_Locx(UE_BS(User_id,:)==1);   % Finding the locatin of the BS which servs the target user
BSy=BS_Locy(UE_BS(User_id,:)==1);
N1=find(d_inf<Threshold_int);

ShadowLOS_int=randn(length(N1),T_slot);
ShadowNLOS_int=randn(length(N1),T_slot);
Rayl_int=random('Rayleigh',1,[length(N1),T_slot]);
OutageTest_int=rand(length(N1),T_slot);
LOSTest_int=rand(length(N1),T_slot);
Outage_State_int=OutageTest_int<PrOUT(d_inf(N1))*ones(1,T_slot);  % If an element of this vecor is 1 then the correspnding user is in outage
Outage_State_int=Outage_State_int*-1e100;
Channel_State_int=LOSTest_int<(PrLOS(d_inf(N1))./(1-PrOUT(d_inf(N1))))*ones(1,T_slot); % If an element of this vecor is 1 then the correspnding user has LOS channel; otherwise it has a NLOS channel.
PL_int=(((plLOS(d_inf(N1))*ones(1,T_slot))+(sigma_LOS.*ShadowLOS_int)).*Channel_State_int.*(1-Outage_State_int))+(((plNLOS(d_inf(N1))*ones(1,T_slot))+(sigma_NLOS.*ShadowNLOS_int)).*(1-Channel_State_int).*(1-Outage_State_int)); % Pathloss + shadowing
PL_int(PL_int>1e50)=inf;
H_int=(Rayl_int.^2)./(10.^(PL_int./10)); % Fading+shadowing+pathloss  AND LOS/NLOS/OUTAGE

Int_FSS=zeros(1,T_slot);
Int_ESP=zeros(1,T_slot);
Int_PSP=zeros(1,T_slot);
Int_OFTS=zeros(1,T_slot);

% int_rep_fss=zeros(4,1);
% MM=0;


for j=1:length(N1)
    
    v1=[BSx-UE_Locx,BSy-UE_Locy];
    v2=[BS_Locx(N1(j))-UE_Locx,BS_Locy(N1(j))-UE_Locy];
    gamma=(pi/180)*acosd(dot(v1,v2)/(norm(v1)*norm(v2)));
    
    if gamma<beam_Ant_UE/2
        X_UE=Na_UE;
        % MM=MM+1;
    else
        X_UE=side_lobe_UE;
    end
    
    
    theta=atan2(UE_Locy-BS_Locy(N1(j)),UE_Locx-BS_Locx(N1(j)));
    theta=(theta>=0)*theta+(theta<0)*(theta+2*pi);
    
    if UE_BS(User_id,N1(j))~=1 % inter-cell interference
        
        psi=abs(Phi_UE_BS(UE_BS(:,N1(j))==1,N1(j))-theta);
        psi=(psi<=pi).*psi+(psi>pi).*(2*pi-psi);
        ISP_number= UE_ISP(UE_BS(:,N1(j))==1);
        S=rand(sum(K(N1(j),:)),T_slot); % Status of the users in the time (On or off at each time slot)
        
        
        if sum(K(N1(j),:))~=0 && strcmp(flag(1),'1') %FSS
            S1=S<1/sum(K(N1(j),:));
            P1=(((psi<(beam_Ant_BS/2))*(floor(Na_BS/floor(360/theta_deg)))+(psi>=(beam_Ant_BS/2))*(side_lobe_BS/floor(360/theta_deg))))*X_UE*(P_transmit_tot/floor(360/theta_deg)).*(label_ss(BStarget)==label_ss(N1(j)));

           if sum(K(N1(j),:))==1
                A1=S1.*(P1*ones(1,T_slot));
            else
                A1=sum(S1.*(P1*ones(1,T_slot)));
            end
            Int_FSS=Int_FSS+H_int(j,:).*A1;
            %                         int_rep_fss(1)=int_rep_fss(1)+sum((X_UE==Na_UE).*(psi<(beam_Ant_BS/2)));
            %                         int_rep_fss(2)=int_rep_fss(2)+sum((X_UE==Na_UE).*(psi>=(beam_Ant_BS/2)));
            %                         int_rep_fss(3)=int_rep_fss(3)+sum((X_UE~=Na_UE).*(psi<(beam_Ant_BS/2)));
            %                         int_rep_fss(4)=int_rep_fss(4)+sum((X_UE~=Na_UE).*(psi>=(beam_Ant_BS/2)));
        end
        
        if K(N1(j),UE_ISP(User_id))~=0 % If there is some users with the same ISP number as the target user then there would be interference for ESP,PSP and OFTS if not the interference is zero
            
            if strcmp(flag(2),'1') %ESP
                S2=S<1/K(N1(j),UE_ISP(User_id));
                P2=((psi<(beam_Ant_BS/2))*(floor(Na_BS/((1+(K(N1(j),-UE_ISP(User_id)+3)>0))*floor(360/theta_deg))))+ (psi>=(beam_Ant_BS/2))*(side_lobe_BS/((1+(K(N1(j),-UE_ISP(User_id)+3)>0))*floor(360/theta_deg)))).*(ISP_number==UE_ISP(User_id))*X_UE*(0.5*P_transmit_tot/(floor(360/theta_deg))).*(label_sp(UE_ISP(User_id),BStarget)==label_sp(ISP_number,N1(j)));
                if sum(K(N1(j),:))==1
                    A2=S2.*(P2*ones(1,T_slot));
                else
                    A2=sum(S2.*(P2*ones(1,T_slot)));
                end
                Int_ESP=Int_ESP+H_int(j,:).*A2;
            end
            
            if strcmp(flag(3),'1') %PSP
                S3=S<1/K(N1(j),UE_ISP(User_id));
                P3=((psi<(beam_Ant_BS/2))*(floor(Na_BS/((1+(K(N1(j),-UE_ISP(User_id)+3)>0))*floor(360/theta_deg))))+ (psi>=(beam_Ant_BS/2))*(side_lobe_BS/((1+(K(N1(j),-UE_ISP(User_id)+3)>0))*floor(360/theta_deg)))).*(ISP_number==UE_ISP(User_id))*X_UE*(MarketS(UE_ISP(User_id))*P_transmit_tot/(floor(360/theta_deg))).*(label_sp(UE_ISP(User_id),BStarget)==label_sp(ISP_number,N1(j))); % The length of every frequency channel is the same
                if sum(K(N1(j),:))==1
                    A3=S3.*(P3*ones(1,T_slot));
                else
                    A3=sum(S3.*(P3*ones(1,T_slot)));
                end
                Int_PSP=Int_PSP+H_int(j,:).*A3;
            end
            
        end
        
        if sum(K(N1(j),:))~=0 && strcmp(flag(7),'1') %OFTS
            MarketS2=[n_ofts,1-n_ofts];
            P4=((psi<(beam_Ant_BS/2))*(floor(Na_BS/(floor(360/theta_deg))))+(psi>=(beam_Ant_BS/2))*(side_lobe_BS/(floor(360/theta_deg)))).*(ISP_number==UE_ISP(User_id))*X_UE*(P_transmit_tot/(floor(360/theta_deg))).*(label_ss(BStarget)==label_ss(N1(j)));
            P5=((psi<(beam_Ant_BS/2))*(floor(Na_BS/(floor(360/theta_deg))))+(psi>=(beam_Ant_BS/2))*(side_lobe_BS/(floor(360/theta_deg)))).*(ISP_number~=UE_ISP(User_id))*X_UE*(P_transmit_tot/(floor(360/theta_deg))).*(label_ss(BStarget)==label_ss(N1(j)));
            S6=rand(1,T_slot)<MarketS2(UE_ISP(User_id));
            
            if (sum(K(N1(j),:))-K(N1(j),UE_ISP(User_id)))~=0
                S5=S<1/(sum(K(N1(j),:))-K(N1(j),UE_ISP(User_id)));
            else
                S5=0;
            end
            
            if K(N1(j),UE_ISP(User_id))~=0
                S4=S<1/K(N1(j),UE_ISP(User_id));
            else
                S4=0;
            end
            
            if sum(K(N1(j),:))==1
                A4=S4.*(P4*ones(1,T_slot));
                A5=S5.*(P5*ones(1,T_slot));
            else
                A4=sum(S4.*(P4*ones(1,T_slot)));
                A5=sum(S5.*(P5*ones(1,T_slot)));
            end
            
            Int_OFTS=Int_OFTS+H_int(j,:).*(S6.*A4+(1-S6).*A5);
        end
        
    end
    
    
end

end