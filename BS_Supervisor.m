function BS_Supervisor (ind_sim,MarketS,BSid,H,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE,P_transmit_tot,Noise,theta_min,OvheadLoss,Lossfact,ro_max,T_slot,N_ISP,N_user_ISP,phi,User_id,ISP_number,frf,flag3,flag5,int_fss,int_esp,int_psp,int_ofts,UE_Locx,UE_Locy,BS_Locx,BS_Locy,Noise_db,theta_deg,ISP_flag,BS_den,UE_den_tot,percent,n_ofts,n_prs)


N_user=length(ISP_number);
alpha=1e-2;   % Scheduling parameter (trade off between short term fairness and long term throughput)

%% Full Spectrum Sharing (FSS)
if ((strcmp(flag3(1),'1') && strcmp(ISP_flag,'0')) || strcmp(ISP_flag,'1') || strcmp(ISP_flag,'2'))
    [User_SNR_FSS_All,User_INR_FSS_All,User_IINR_FSS_All,User_OINR_FSS_All,User_SINR_FSS_All,User_SINR_FSS,User_Rate_FSS,User_Rate_FSS_SNR,User_Rate_FSS_SIINR,User_Rate_FSS_SOINR,ISP_Rate_FSS,User_per_slot_FSS,User_Share_FSS]=Full_Spectrum_Sharing(int_fss,H,phi,T_slot,theta_min,N_ISP,ISP_number,P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE);
    User_Rate_FSS=User_Rate_FSS./frf;
    User_Rate_FSS_SNR=User_Rate_FSS_SNR./frf;
    User_Rate_FSS_SIINR=User_Rate_FSS_SIINR./frf;
    User_Rate_FSS_SOINR=User_Rate_FSS_SOINR./frf;
    ISP_Rate_FSS=ISP_Rate_FSS./frf;
end

%% Proportional Resource Sharing (PRS)
if ((strcmp(flag3(8),'1') && strcmp(ISP_flag,'0'))) 
    [User_SNR_PRS_All,User_INR_PRS_All,User_IINR_PRS_All,User_OINR_PRS_All,User_SINR_PRS_All,User_SINR_PRS,User_Rate_PRS,User_Rate_PRS_SNR,User_Rate_PRS_SIINR,User_Rate_PRS_SOINR,ISP_Rate_PRS,User_per_slot_PRS,User_Share_PRS]=Proportional_Resource_Sharing(int_fss,H,phi,T_slot,theta_min,N_ISP,ISP_number,P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE,percent,n_prs);
    User_Rate_PRS=User_Rate_PRS./frf;
    User_Rate_PRS_SNR=User_Rate_PRS_SNR./frf;
    User_Rate_PRS_SIINR=User_Rate_PRS_SIINR./frf;
    User_Rate_PRS_SOINR=User_Rate_PRS_SOINR./frf;
    ISP_Rate_PRS=ISP_Rate_PRS./frf;
end

%% Even Spectrum Partitioning (ESP)
if strcmp(flag3(2),'1') && strcmp(ISP_flag,'0')
     Spectrum_sharing_weight=[0.5 0.5]; % Share of each operator in non-spectrum sharing case
     [User_SNR_ESP_All,User_INR_ESP_All,User_IINR_ESP_All,User_OINR_ESP_All,User_SINR_ESP_All,User_SINR_ESP,User_Rate_ESP,User_Rate_ESP_SNR,User_Rate_ESP_SIINR,User_Rate_ESP_SOINR,ISP_Rate_ESP,User_per_slot_ESP,User_Share_ESP]=Spectrum_Partitioning(int_esp,H,phi,T_slot,theta_min,Spectrum_sharing_weight,N_user_ISP,find(ISP_number==1),find(ISP_number==2),P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE);
     User_Rate_ESP=User_Rate_ESP./frf;
     User_Rate_ESP_SNR=User_Rate_ESP_SNR./frf;
     User_Rate_ESP_SIINR=User_Rate_ESP_SIINR./frf;
     User_Rate_ESP_SOINR=User_Rate_ESP_SOINR./frf;
     ISP_Rate_ESP=ISP_Rate_ESP./frf;
end

%% Proportional Spectrum Partitioning (PSP)
if strcmp(flag3(3),'1')&& strcmp(ISP_flag,'0')
     Spectrum_sharing_weight=MarketS; % Share of each operator in non-spectrum sharing case
     [User_SNR_PSP_All,User_INR_PSP_All,User_IINR_PSP_All,User_OINR_PSP_All,User_SINR_PSP_All,User_SINR_PSP,User_Rate_PSP,User_Rate_PSP_SNR,User_Rate_PSP_SIINR,User_Rate_PSP_SOINR,ISP_Rate_PSP,User_per_slot_PSP,User_Share_PSP]=Spectrum_Partitioning(int_psp,H,phi,T_slot,theta_min,Spectrum_sharing_weight,N_user_ISP,find(ISP_number==1),find(ISP_number==2),P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE);
     User_Rate_PSP=User_Rate_PSP./frf;
     User_Rate_PSP_SNR=User_Rate_PSP_SNR./frf;
     User_Rate_PSP_SIINR=User_Rate_PSP_SIINR./frf;
     User_Rate_PSP_SOINR=User_Rate_PSP_SOINR./frf;
     ISP_Rate_PSP=ISP_Rate_PSP./frf;
end

% %% Partial Spectrum Sharing - 25% Fully opportunistic selection (PSS-25)
% if strcmp(flag3(4),'1') && strcmp(ISP_flag,'0')
%     Sharing_weight=0.25;
%     [User_INR_PSS_25_All,User_SINR_PSS_25_All,User_SINR_PSS_25,User_Rate_PSS_25,ISP_Rate_PSS_25,User_per_slot_PSS_25,User_Share_PSS_25,Counter_ISP_PSS_25]=Partial_Spectrum_Sharing(int_share_pss25,H,phi,T_slot,theta_min,Sharing_weight,N_user_ISP,find(ISP_number==1),find(ISP_number==2),P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise,alpha,Na_BS,side_lobe_BS);
% end
% 
% %% Partial Spectrum Sharing - 50% Fully opportunistic selection (PSS-50)
% if strcmp(flag3(5),'1') && strcmp(ISP_flag,'0')
%     Sharing_weight=0.50;
%     [User_INR_PSS_50_All,User_SINR_PSS_50_All,User_SINR_PSS_50,User_Rate_PSS_50,ISP_Rate_PSS_50,User_per_slot_PSS_50,User_Share_PSS_50,Counter_ISP_PSS_50]=Partial_Spectrum_Sharing(int_share_pss50,H,phi,T_slot,theta_min,Sharing_weight,N_user_ISP,find(ISP_number==1),find(ISP_number==2),P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise,alpha,Na_BS,side_lobe_BS);
% end
% 
% %% Partial Spectrum Sharing - 75% Fully opportunistic selection (PSS-75)
% if strcmp(flag3(6),'1') && strcmp(ISP_flag,'0')
%     Sharing_weight=0.75;
%     [User_INR_PSS_75_All,User_SINR_PSS_75_All,User_SINR_PSS_75,User_Rate_PSS_75,ISP_Rate_PSS_75,User_per_slot_PSS_75,User_Share_PSS_75,Counter_ISP_PSS_75]=Partial_Spectrum_Sharing(int_share_pss75,H,phi,T_slot,theta_min,Sharing_weight,N_user_ISP,find(ISP_number==1),find(ISP_number==2),P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise,alpha,Na_BS,side_lobe_BS);
% end

%% Opportunistic Fair Time Sharing (OFTS)
if strcmp(flag3(7),'1') && strcmp(ISP_flag,'0')
    %Time_Sharing_weight=MarketS;
    Time_Sharing_weight=[n_ofts,1-n_ofts];
    [User_SNR_OFTS_All,User_INR_OFTS_All,User_IINR_OFTS_All,User_OINR_OFTS_All,User_SINR_OFTS_All,User_SINR_OFTS,User_Rate_OFTS,User_Rate_OFTS_SNR,User_Rate_OFTS_SIINR,User_Rate_OFTS_SOINR,ISP_Rate_OFTS,User_per_slot_OFTS,User_Share_OFTS,Counter_ISP_OFTS]=Opportunistic_Fair_Time_Sharing(int_ofts,H,phi,T_slot,theta_min,Time_Sharing_weight,N_user_ISP,find(ISP_number==1),find(ISP_number==2),P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE);
    User_Rate_OFTS=User_Rate_OFTS./frf;
    User_Rate_OFTS_SNR=User_Rate_OFTS_SNR./frf;
    User_Rate_OFTS_SIINR=User_Rate_OFTS_SIINR./frf;
    User_Rate_OFTS_SOINR=User_Rate_OFTS_SOINR./frf;
    ISP_Rate_OFTS=ISP_Rate_OFTS./frf;
end


%% Not BS sharing, not spectrum sharing case
if (strcmp(ISP_flag,'-1'))
    [User_SNR_ESP_All,User_INR_ESP_All,User_IINR_ESP_All,User_OINR_ESP_All,User_SINR_ESP_All,User_SINR_ESP,User_Rate_ESP,User_Rate_ESP_SNR,User_Rate_ESP_SIINR,User_Rate_ESP_SOINR,ISP_Rate_ESP,User_per_slot_ESP,User_Share_ESP]=Full_Spectrum_Sharing(int_esp,H,phi,T_slot,theta_min,N_ISP,ISP_number,P_transmit_tot,ro_max,OvheadLoss,Lossfact,0.5*Noise,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE);
    User_Rate_ESP=0.5*User_Rate_ESP./frf;
    User_Rate_ESP_SNR=0.5*User_Rate_ESP_SNR./frf;
    User_Rate_ESP_SIINR=0.5*User_Rate_ESP_SIINR./frf;
    User_Rate_ESP_SOINR=0.5*User_Rate_ESP_SOINR./frf;
    ISP_Rate_ESP=0.5*ISP_Rate_ESP./frf;
        
    [User_SNR_PSP_All,User_INR_PSP_All,User_IINR_PSP_All,User_OINR_PSP_All,User_SINR_PSP_All,User_SINR_PSP,User_Rate_PSP,User_Rate_PSP_SNR,User_Rate_PSP_SIINR,User_Rate_PSP_SOINR,ISP_Rate_PSP,User_per_slot_PSP,User_Share_PSP]=Full_Spectrum_Sharing(int_psp,H,phi,T_slot,theta_min,N_ISP,ISP_number,P_transmit_tot,ro_max,OvheadLoss,Lossfact,MarketS(1)*Noise,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE);
    User_Rate_PSP=MarketS(1)*User_Rate_PSP./frf;
    User_Rate_PSP_SNR=MarketS(1)*User_Rate_PSP_SNR./frf;
    User_Rate_PSP_SIINR=MarketS(1)*User_Rate_PSP_SIINR./frf;
    User_Rate_PSP_SOINR=MarketS(1)*User_Rate_PSP_SOINR./frf;
    ISP_Rate_PSP=MarketS(1)*ISP_Rate_PSP./frf;
end
if (strcmp(ISP_flag,'-2'))
    [User_SNR_ESP_All,User_INR_ESP_All,User_IINR_ESP_All,User_OINR_ESP_All,User_SINR_ESP_All,User_SINR_ESP,User_Rate_ESP,User_Rate_ESP_SNR,User_Rate_ESP_SIINR,User_Rate_ESP_SOINR,ISP_Rate_ESP,User_per_slot_ESP,User_Share_ESP]=Full_Spectrum_Sharing(int_esp,H,phi,T_slot,theta_min,N_ISP,ISP_number,P_transmit_tot,ro_max,OvheadLoss,Lossfact,0.5*Noise,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE);
    User_Rate_ESP=0.5*User_Rate_ESP./frf;
    User_Rate_ESP_SNR=0.5*User_Rate_ESP_SNR./frf;
    User_Rate_ESP_SIINR=0.5*User_Rate_ESP_SIINR./frf;
    User_Rate_ESP_SOINR=0.5*User_Rate_ESP_SOINR./frf;
    ISP_Rate_ESP=0.5*ISP_Rate_ESP./frf;
        
    [User_SNR_PSP_All,User_INR_PSP_All,User_IINR_PSP_All,User_OINR_PSP_All,User_SINR_PSP_All,User_SINR_PSP,User_Rate_PSP,User_Rate_PSP_SNR,User_Rate_PSP_SIINR,User_Rate_PSP_SOINR,ISP_Rate_PSP,User_per_slot_PSP,User_Share_PSP]=Full_Spectrum_Sharing(int_psp,H,phi,T_slot,theta_min,N_ISP,ISP_number,P_transmit_tot,ro_max,OvheadLoss,Lossfact,MarketS(2)*Noise,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE);
    User_Rate_PSP=MarketS(2)*User_Rate_PSP./frf;
    User_Rate_PSP_SNR=MarketS(2)*User_Rate_PSP_SNR./frf;
    User_Rate_PSP_SIINR=MarketS(2)*User_Rate_PSP_SIINR./frf;
    User_Rate_PSP_SOINR=MarketS(2)*User_Rate_PSP_SOINR./frf;
    ISP_Rate_PSP=MarketS(2)*ISP_Rate_PSP./frf;;
end


%% Saving User Data

 
user_filename = ['dat-user/User_'     ...
    num2str(MarketS(1)) '_' ...
    num2str(MarketS(2)) '_' ...
    num2str(BS_den) '_'...
    num2str(UE_den_tot) '_'...
    num2str(theta_deg) '_'   ...
    num2str(Noise_db) '_'    ...
    flag3 '_' ...
    flag5 '_' ...
    ISP_flag '_' ...
    num2str(n_ofts) '_'...
    num2str(n_prs) '_' ...
    num2str(ind_sim) '.csv'];

fileID = fopen(user_filename,'w');
formatSpec = 'Sim ID,User ID,Parameter,Value\n';
fprintf(fileID,formatSpec);

for i=1:N_user
    
    % General user data
    formatSpec = '%d,%d,xLoc,%f\n';
    fprintf(fileID,formatSpec,ind_sim,User_id(i),UE_Locx(i));
    formatSpec = '%d,%d,yLoc,%f\n';
    fprintf(fileID,formatSpec,ind_sim,User_id(i),UE_Locy(i));
    formatSpec = '%d,%d,ispNumberUe,%d\n';
    fprintf(fileID,formatSpec,ind_sim,User_id(i),ISP_number(i));
    
    % FSS
    if ((strcmp(flag3(1),'1') && strcmp(ISP_flag,'0')) || strcmp(ISP_flag,'1') || strcmp(ISP_flag,'2'))
        formatSpec = '%d,%d,tputSs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_FSS(i));
        formatSpec = '%d,%d,tputSnrSs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_FSS_SNR(i));
        formatSpec = '%d,%d,tputSiinrSs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_FSS_SIINR(i));
        formatSpec = '%d,%d,tputSoinrSs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_FSS_SOINR(i));
        formatSpec = '%d,%d,sinrSs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SINR_FSS(i));
        formatSpec = '%d,%d,snrSsReal,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SNR_FSS_All(i));
        formatSpec = '%d,%d,sinrSsReal,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SINR_FSS_All(i));
        formatSpec = '%d,%d,inrSs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_INR_FSS_All(i));
        formatSpec = '%d,%d,iinrSs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_IINR_FSS_All(i));
        formatSpec = '%d,%d,oinrSs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_OINR_FSS_All(i));
        formatSpec = '%d,%d,airTimeSs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Share_FSS(i));
    end
    
    % PRS
    if ((strcmp(flag3(8),'1') && strcmp(ISP_flag,'0')))
        formatSpec = '%d,%d,tputPrs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_PRS(i));
        formatSpec = '%d,%d,tputSnrPrs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_PRS_SNR(i));
        formatSpec = '%d,%d,tputSiinrPrs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_PRS_SIINR(i));
        formatSpec = '%d,%d,tputSoinrPrs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_PRS_SOINR(i));
        formatSpec = '%d,%d,sinrPrs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SINR_PRS(i));
        formatSpec = '%d,%d,snrPrsReal,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SNR_PRS_All(i));
        formatSpec = '%d,%d,sinrPrsReal,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SINR_PRS_All(i));
        formatSpec = '%d,%d,inrPrs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_INR_PRS_All(i));
        formatSpec = '%d,%d,iinrPrs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_IINR_PRS_All(i));
        formatSpec = '%d,%d,oinrPrs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_OINR_PRS_All(i));
        formatSpec = '%d,%d,airTimePrs,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Share_PRS(i));
    end
    
    % ESP
    if ((strcmp(flag3(2),'1') && strcmp(ISP_flag,'0'))|| (strcmp(ISP_flag,'-1') || strcmp(ISP_flag,'-2')))
        formatSpec = '%d,%d,tputEsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_ESP(i));
        formatSpec = '%d,%d,tputSnrEsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_ESP_SNR(i));
        formatSpec = '%d,%d,tputSiinrEsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_ESP_SIINR(i));
        formatSpec = '%d,%d,tputSoinrEsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_ESP_SOINR(i));
        formatSpec = '%d,%d,sinrEsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SINR_ESP(i));
        formatSpec = '%d,%d,snrEspReal,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SNR_ESP_All(i));
        formatSpec = '%d,%d,sinrEspReal,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SINR_ESP_All(i));
        formatSpec = '%d,%d,inrEsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_INR_ESP_All(i));
        formatSpec = '%d,%d,iinrEsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_IINR_ESP_All(i));
        formatSpec = '%d,%d,oinrEsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_OINR_ESP_All(i));
        formatSpec = '%d,%d,airTimeEsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Share_ESP(i));
    end
    
    % PSP
    if ((strcmp(flag3(3),'1') && strcmp(ISP_flag,'0'))||(strcmp(ISP_flag,'-1') || strcmp(ISP_flag,'-2')))
        formatSpec = '%d,%d,tputPsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_PSP(i));
        formatSpec = '%d,%d,tputSnrPsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_PSP_SNR(i));
        formatSpec = '%d,%d,tputSiinrPsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_PSP_SIINR(i));
        formatSpec = '%d,%d,tputSoinrPsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_PSP_SOINR(i));
        formatSpec = '%d,%d,sinrPsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SINR_PSP(i));
        formatSpec = '%d,%d,snrPspReal,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SNR_PSP_All(i));
        formatSpec = '%d,%d,sinrPspReal,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SINR_PSP_All(i));
        formatSpec = '%d,%d,inrPsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_INR_PSP_All(i));
        formatSpec = '%d,%d,iinrPsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_IINR_PSP_All(i));
        formatSpec = '%d,%d,oinrPsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_OINR_PSP_All(i));
        formatSpec = '%d,%d,airTimePsp,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Share_PSP(i));
    end
    
    % OFTS
    if strcmp(flag3(7),'1') && strcmp(ISP_flag,'0')
        formatSpec = '%d,%d,tputOfts,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_OFTS(i));
        formatSpec = '%d,%d,tputSnrOfts,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_OFTS_SNR(i));
        formatSpec = '%d,%d,tputSiinrOfts,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_OFTS_SIINR(i));
        formatSpec = '%d,%d,tputSoinrOfts,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Rate_OFTS_SOINR(i));
        formatSpec = '%d,%d,sinrOfts,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SINR_OFTS(i));
        formatSpec = '%d,%d,snrOftsReal,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SNR_OFTS_All(i));
        formatSpec = '%d,%d,sinrOftsReal,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_SINR_OFTS_All(i));
        formatSpec = '%d,%d,inrOfts,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_INR_OFTS_All(i));
        formatSpec = '%d,%d,iinrOfts,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_IINR_OFTS_All(i));
        formatSpec = '%d,%d,oinrOfts,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_OINR_OFTS_All(i));
        formatSpec = '%d,%d,airTimeOfts,%f\n';
        fprintf(fileID,formatSpec,ind_sim,User_id(i),User_Share_OFTS(i));
    end
    
end

fclose(fileID);


%% Saving BS Data

bs_filename = ['dat-bs/BS_'     ...
    num2str(MarketS(1)) '_' ...
    num2str(MarketS(2)) '_' ...
    num2str(BS_den) '_'...
    num2str(UE_den_tot) '_'...
    num2str(theta_deg) '_'   ...
    num2str(Noise_db) '_'    ...
    flag3 '_' ...
    flag5 '_' ...
    ISP_flag '_' ...
    num2str(n_ofts) '_'...
    num2str(n_prs) '_' ...
    num2str(ind_sim) '.csv'];

fileID = fopen(bs_filename,'w');
formatSpec = 'Sim ID,BS ID,Parameter,Value\n';
fprintf(fileID,formatSpec);

% General BS Data
formatSpec = '%d,%d,Xloc,%f\n';
fprintf(fileID,formatSpec,ind_sim,BSid,BS_Locx);
formatSpec = '%d,%d,Yloc,%f\n';
fprintf(fileID,formatSpec,ind_sim,BSid,BS_Locy);
formatSpec = '%d,%d,user1Tot,%d\n';
fprintf(fileID,formatSpec,ind_sim,BSid,sum(ISP_number==1));
formatSpec = '%d,%d,user2Tot,%d\n';
fprintf(fileID,formatSpec,ind_sim,BSid,sum(ISP_number==2));
formatSpec = '%d,%d,ispNumberBs,%d\n';
ISP_flag_num=str2double(ISP_flag);
fprintf(fileID,formatSpec,ind_sim,BSid,ISP_flag_num);



% FSS
if ((strcmp(flag3(1),'1') && strcmp(ISP_flag,'0')) || strcmp(ISP_flag,'1') || strcmp(ISP_flag,'2'))
    formatSpec = '%d,%d,netTput1Fss,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,ISP_Rate_FSS(1));
    formatSpec = '%d,%d,netTput2Fss,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,ISP_Rate_FSS(2));
    formatSpec = '%d,%d,avgActiveUe1TimeFss,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,User_per_slot_FSS(1));
    formatSpec = '%d,%d,avgActiveUe2TimeFss,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,User_per_slot_FSS(2));
end

% PRS
if ((strcmp(flag3(8),'1') && strcmp(ISP_flag,'0')))
    formatSpec = '%d,%d,netTput1Prs,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,ISP_Rate_PRS(1));
    formatSpec = '%d,%d,netTput2Prs,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,ISP_Rate_PRS(2));
    formatSpec = '%d,%d,avgActiveUe1TimePrs,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,User_per_slot_PRS(1));
    formatSpec = '%d,%d,avgActiveUe2TimePrs,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,User_per_slot_PRS(2));
end

% ESP
if ((strcmp(flag3(2),'1') && strcmp(ISP_flag,'0'))|| (strcmp(ISP_flag,'-1') || strcmp(ISP_flag,'-2')))
    formatSpec = '%d,%d,netTput1Esp,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,ISP_Rate_ESP(1));
    formatSpec = '%d,%d,netTput2Esp,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,ISP_Rate_ESP(2));
    formatSpec = '%d,%d,avgActiveUe1TimeEsp,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,User_per_slot_ESP(1));
    formatSpec = '%d,%d,avgActiveUe2TimeEsp,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,User_per_slot_ESP(2));
    
end

% PSP
if ((strcmp(flag3(3),'1') && strcmp(ISP_flag,'0'))||(strcmp(ISP_flag,'-1') || strcmp(ISP_flag,'-2')))
    formatSpec = '%d,%d,netTput1Psp,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,ISP_Rate_PSP(1));
    formatSpec = '%d,%d,netTput2Psp,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,ISP_Rate_PSP(2));
    formatSpec = '%d,%d,avgActiveUe1TimePsp,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,User_per_slot_PSP(1));
    formatSpec = '%d,%d,avgActiveUe2TimePsp,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,User_per_slot_PSP(2));
end

% OFTS
if strcmp(flag3(7),'1') && strcmp(ISP_flag,'0')
    formatSpec = '%d,%d,netTput1Ofts,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,ISP_Rate_OFTS(1));
    formatSpec = '%d,%d,netTput2Ofts,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,ISP_Rate_OFTS(2));
    formatSpec = '%d,%d,avgActiveUe1TimeOfts,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,User_per_slot_OFTS(1));
    formatSpec = '%d,%d,avgActiveUe2TimeOfts,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,User_per_slot_OFTS(2));
    formatSpec = '%d,%d,slots1Ofts,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,Counter_ISP_OFTS(1));
    formatSpec = '%d,%d,slots2Ofts,%f\n';
    fprintf(fileID,formatSpec,ind_sim,BSid,Counter_ISP_OFTS(2));
end

fclose(fileID);

end
