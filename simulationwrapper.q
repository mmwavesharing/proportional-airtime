#!/bin/bash
#PBS -l mem=6GB
#PBS -l walltime=8:00:00
#PBS -l nodes=1:ppn=1
#PBS -j oe
#PBS -t 1-200

module purge
module load matlab/2015b
 
cd $SCRATCH/basic-multicell-simulation

matlab -nodisplay -r "simulationwrapper, exit"

