function[User_SNR_All,User_INR_All,User_IINR_All,User_OINR_All,User_SINR_All,User_SINR,User_Rate,User_Rate_SNR,User_Rate_SIINR,User_Rate_SOINR,Rate_ISP,User_per_slot,User_Share,Counter_ISP]=Opportunistic_Fair_Time_Sharing(IntfcO,H,phi,T_slot,theta_min,Time_Sharing_weight,N_user_ISP,user_vector1,user_vector2,P_transmit_tot,ro_max,OvheadLoss,Lossfact,Noise_net,alpha,Na_BS,Na_UE,side_lobe_BS,side_lobe_UE)

n1=N_user_ISP(1);     % Number of users of ISP1
n2=N_user_ISP(2);     % Number of users of ISP2

if n1>0
  w1=ones(1,N_user_ISP(1))'./N_user_ISP(1); % Scheduling weights of the users of ISP 1
end

if n2>0  
   w2=ones(1,N_user_ISP(2))'./N_user_ISP(2); % Scheduling weights of the users of ISP 2
end
% Scheduling parameters
b1=zeros([N_user_ISP(1),1]); % Bucket vector of users of ISP 1
b2=zeros([N_user_ISP(2),1]); % Bucket vector of users of ISP 2
B1=0; % Bucket value of ISP1
B2=0; % Bucket value of ISP2

Counter1=zeros([N_user_ISP(1),1]); % Counter vector for users of ISP 1 to measure the fairness
Counter2=zeros([N_user_ISP(2),1]); % Counter vector for users of ISP 2 to measure the fairness
User_Rate1=zeros(N_user_ISP(1),1); % Rate vector of users of ISP 1
User_Rate2=zeros(N_user_ISP(2),1); % Rate vector of users of ISP 2

User_Rate_SNR1=zeros(N_user_ISP(1),1); % Rate vector of users of ISP 1
User_Rate_SNR2=zeros(N_user_ISP(2),1); % Rate vector of users of ISP 2
User_Rate_SIINR1=zeros(N_user_ISP(1),1); % Rate vector of users of ISP 1
User_Rate_SIINR2=zeros(N_user_ISP(2),1); % Rate vector of users of ISP 2
User_Rate_SOINR1=zeros(N_user_ISP(1),1); % Rate vector of users of ISP 1
User_Rate_SOINR2=zeros(N_user_ISP(2),1); % Rate vector of users of ISP 2


User_SNR1_All=zeros(N_user_ISP(1),1);  % Average SNR vector of users of ISP 1 when they are scheduled
User_SNR2_All=zeros(N_user_ISP(2),1);  % Average SNR vector of users of ISP 2 when they are scheduled
User_SINR1=zeros(N_user_ISP(1),1);  % Average SINR vector of users of ISP 1 when they are scheduled
User_SINR2=zeros(N_user_ISP(2),1);  % Average SINR vector of users of ISP 2 when they are scheduled
User_SINR1_All=zeros(N_user_ISP(1),1);  % Average SINR vector of users of ISP 1
User_SINR2_All=zeros(N_user_ISP(2),1);  % Average SINR vector of users of ISP 2
User_INR1_All=zeros(N_user_ISP(1),1);  % Average INR vector of users of ISP 1
User_INR2_All=zeros(N_user_ISP(2),1);  % Average INR vector of users of ISP 2
User_IINR1_All=zeros(N_user_ISP(1),1);  % Average intra-cell INR vector of users of ISP 1
User_IINR2_All=zeros(N_user_ISP(2),1);  % Average intra-cell INR vector of users of ISP 2
User_OINR1_All=zeros(N_user_ISP(1),1);  % Average inter-cell INR vector of users of ISP 1
User_OINR2_All=zeros(N_user_ISP(2),1);  % Average inter-cell INR vector of users of ISP 2


Counter_ISP=zeros(2,1);
Rate_ISP1=0;
Rate_ISP2=0;
Noise=Noise_net; % Noise power (whenever an ISP takes the spectrum, it takes the whole spectrum )
NUmax=floor((2*pi)/theta_min); % Maximum number of active users
P_transmit=P_transmit_tot/NUmax;
IntfcO1=IntfcO(user_vector1,:);
IntfcO2=IntfcO(user_vector2,:);


for i=1:1:T_slot
    
    
    % Operator 1 scheduling
    
    if n1>0
        H1=H(user_vector1,i);
        IntInt1=(side_lobe_BS/NUmax)*Na_UE*(NUmax-1)*H1*P_transmit;  % Amount of intra-cell interference for ISP1 assuming maximum number of users are active
        R1=min((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit*H1)./(Noise+IntInt1)))),ro_max);
        phi1=phi(user_vector1);
        
        User_index1=zeros(N_user_ISP(1),1); % Auxilliary vector in scheduling, we are done in each time slot if all of the elements of it become 1
        j1=1; % Auxiliary counter
        k1=1; % Auxiliary counter
        U1=R1+(alpha*b1); %Algorithm A : Opportunistic Fair Scheduling
        [A1,C1(j1)]=max(U1);
        Ch1(k1)=C1(j1);
        User_index1(C1(j1))=1;
        Th1(j1)=phi1(C1(j1));
        R1(C1(j1))=-inf; % Eliminating the selected user from the list for the current time slot because it cannot be selected more than once
        bb1=b1; % Back up for the bucket vector which will be used when the ISP loses the competition and does not take the channel
        b1(C1(j1))=b1(C1(j1))-1; % Updating the bucket(credit) of the selected user
        bc1=Counter1; % Back up for the users' counter vector which will be used when the ISP loses the competition and does not take the channel
        Counter1(C1(j1))=Counter1(C1(j1))+1; % updating the counter of the selected user
        b1=b1+w1; % updating the bucket(credit) of all the users based on the scheduling weights
        j1=j1+1;
        k1=k1+1;
        U1=R1+(alpha*b1); % Recalculate the objective function
        
        while  sum(User_index1)<N_user_ISP(1)
            
            %         if j1>(pi/theta_min)  %Algorithm B : Pure Opportunistic Scheduling
            %             [A1,C1(j1)]=max(R1);
            %             User_index1(C1(j1))=1;
            %             R1(C1(j1))=-inf;
            %             cond=0;
            %             if (min(abs(phi1(C1(j1))-Th1))<theta_min) || (min((2*pi)-abs(phi1(C1(j1))-Th1))<theta_min)
            %                 cond=1;
            %             end
            %
            %             if cond==0
            %                 Ch1(k1)=C1(j1);
            %                 Counter1(C1(j1))=Counter1(C1(j1))+1;
            %                 Th1(j1)=phi1(C1(j1));
            %                 j1=j1+1;
            %                 k1=k1+1;
            %             end
            %
            %         else
            
            [A1,C1(j1)]=max(U1);
            User_index1(C1(j1))=1;
            R1(C1(j1))=-inf;
            cond=0;
            if (min(abs(phi1(C1(j1))-Th1))<theta_min) || (min((2*pi)-abs(phi1(C1(j1))-Th1))<theta_min)
                cond=1;
            end
            
            if cond==0
                Ch1(k1)=C1(j1);
                Counter1(C1(j1))=Counter1(C1(j1))+1;
                b1(C1(j1))=b1(C1(j1))-1;
                b1=b1+w1;
                U1=R1+(alpha*b1);
                Th1(j1)=phi1(C1(j1));
                j1=j1+1;
                k1=k1+1;
            else
                U1(C1(j1))=-inf;
            end
            
            %end
            
        end
        %Th/(2*pi)*360
    end
    
    
    % Operator 2 scheduling
    if n2>0
        H2=H(user_vector2,i);
        IntInt2=(side_lobe_BS/NUmax)*Na_UE*(NUmax-1)*H2*P_transmit;  % Amount of intra-cell interference for ISP2 assuming maximum number of users are active
        R2=min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit*H2)./(Noise+IntInt2))))),ro_max);
        phi2=phi(user_vector2);
        
        User_index2=zeros(N_user_ISP(2),1); % Auxilliary vector in scheduling, we are done in each time slot if all of the elements of it become 1
        j2=1;
        k2=1;
        U2=R2+(alpha*b2); %Algorithm A : Opportunistic Fair Scheduling
        [A2,C2(j2)]=max(U2);
        Ch2(k2)=C2(j2);
        User_index2(C2(j2))=1;
        Th2(j2)=phi2(C2(j2));
        R2(C2(j2))=-inf; % Eliminating the selected user from the list for the current time slot because it cannot be selected more than once
        bb2=b2; % Back up for the bucket vector which will be used when the ISP loses the competition and does not take the channel
        b2(C2(j2))=b2(C2(j2))-1; % Updating the bucket(credit) of the selected user
        bc2=Counter2; % Back up for the users' counter vector which will be used when the ISP loses the competition and does not take the channel
        Counter2(C2(j2))=Counter2(C2(j2))+1; % updating the counter of the selected user
        b2=b2+w2; % updating the bucket(credit) of all the users based on the scheduling weights
        j2=j2+1;
        k2=k2+1;
        U2=R2+(alpha*b2); % Recalculate the objective function
        
        while  sum(User_index2)<N_user_ISP(2)
            
            %         if j2>(pi/theta_min)  %Algorithm B : Pure Opportunistic Scheduling
            %             [A2,C2(j2)]=max(R2);
            %             User_index2(C2(j2))=1;
            %             R2(C2(j2))=-inf;
            %             cond=0;
            %             if (min(abs(phi2(C2(j2))-Th2))<theta_min) || (min((2*pi)-abs(phi2(C2(j2))-Th2))<theta_min)
            %                 cond=1;
            %             end
            %
            %             if cond==0
            %                 Ch2(k2)=C2(j2);
            %                 Counter2(C2(j2))=Counter2(C2(j2))+1;
            %                 Th2(j2)=phi2(C2(j2));
            %                 j2=j2+1;
            %                 k2=k2+1;
            %             end
            %
            %         else
            
            [A2,C2(j2)]=max(U2);
            User_index2(C2(j2))=1;
            R2(C2(j2))=-inf;
            cond=0;
            if (min(abs(phi2(C2(j2))-Th2))<theta_min) || (min((2*pi)-abs(phi2(C2(j2))-Th2))<theta_min)
                cond=1;
            end
            
            if cond==0
                Ch2(k2)=C2(j2);
                Counter2(C2(j2))=Counter2(C2(j2))+1;
                b2(C2(j2))=b2(C2(j2))-1;
                b2=b2+w2;
                U2=R2+(alpha*b2);
                Th2(j2)=phi2(C2(j2));
                j2=j2+1;
                k2=k2+1;
            else
                U2(C2(j2))=-inf;
            end
            
            %end
            
        end
    end
    
    BF_All= Na_UE*max(floor(Na_BS/NUmax),1);          % Beamforming gain when the maximum number of the users are scheduled
    P_transmit1_All=BF_All*P_transmit_tot./NUmax; % Equivalet transmitting power for users of ISP1 after beamforming
    P_transmit2_All=BF_All*P_transmit_tot./NUmax; % Equivalet transmitting power for users of ISP2 after beamforming
    
    
    if n1>0
        Nau1=length(Ch1); % Number of active (scheduled) users of ISP1
        BF1= Na_UE*max(floor(Na_BS/Nau1),1); % Beamforming gain for ISP1
        P_transmit1=BF1*P_transmit_tot./Nau1; % Equivalet transmitting power for users of ISP1 after beamforming
        IntfcI1=(side_lobe_BS/Nau1)*Na_UE*(Nau1-1)*H1*P_transmit_tot./Nau1;  % Inter-cell interference for ISP1
        Rate1=min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit1.*H1(Ch1))./(Noise+IntfcI1(Ch1)+IntfcO1(Ch1,i)))))),ro_max);
        
        User_SNR1_All=User_SNR1_All+((P_transmit1_All*H1)./Noise);
        User_SINR1_All=User_SINR1_All+((P_transmit1_All*H1)./(Noise+IntInt1+IntfcO1(:,i)));
        User_INR1_All=User_INR1_All+((IntInt1+IntfcO1(:,i))./Noise);
        User_IINR1_All=User_IINR1_All+(IntInt1./Noise);
        User_OINR1_All=User_OINR1_All+(IntfcO1(:,i)./Noise);
    end
    
    if n2>0
        Nau2=length(Ch2); % Number of active (scheduled) users of ISP2
        BF2= Na_UE*max(floor(Na_BS/Nau2),1); % Beamforming gain for ISP1
        P_transmit2=BF2*P_transmit_tot./Nau2; % Equivalet transmitting power for users of ISP2 after beamforming
        IntfcI2=(side_lobe_BS/Nau2)*Na_UE*(Nau2-1)*H2*P_transmit_tot./Nau2;  % Inter-cell interference for ISP2
        Rate2=min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit2.*H2(Ch2))./(Noise+IntfcI2(Ch2)+IntfcO2(Ch2,i)))))),ro_max);
        
        User_SNR2_All=User_SNR2_All+((P_transmit2_All*H2)./Noise);
        User_SINR2_All=User_SINR2_All+((P_transmit2_All*H2)./(Noise+IntInt2+IntfcO2(:,i)));
        User_INR2_All=User_INR2_All+((IntInt2+IntfcO2(:,i))./Noise);
        User_IINR2_All=User_IINR2_All+(IntInt2./Noise);
        User_OINR2_All=User_OINR2_All+(IntfcO2(:,i)./Noise);
    end
    
    
    if n1>0 && n2>0
        
        if sum(Rate1)+alpha*B1>sum(Rate2)+alpha*B2 % ISP1 will take the channel
            
            Counter_ISP(1)=Counter_ISP(1)+1;
            B1=B1+Time_Sharing_weight(1)-1; % Updating the bucket of ISP1
            B2=B2+Time_Sharing_weight(2); % Updating the bucket of ISP2
            b2=bb2; % Retrieving the buckets values for the users of ISP2
            Counter2=bc2; % Retrieving the counter values for the users of ISP2
            Rate_ISP1=Rate_ISP1+sum(Rate1);
            User_Rate1(Ch1)=User_Rate1(Ch1)+Rate1;
            User_SINR1(Ch1)=User_SINR1(Ch1)+((P_transmit1*H1(Ch1))./(Noise+IntfcI1(Ch1)+IntfcO1(Ch1,i)));
            
            User_Rate_SNR1(Ch1)=User_Rate_SNR1(Ch1)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit1.*H1(Ch1))./(Noise))))),ro_max);
            User_Rate_SIINR1(Ch1)=User_Rate_SIINR1(Ch1)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit1.*H1(Ch1))./(Noise+IntfcI1(Ch1)))))),ro_max);
            User_Rate_SOINR1(Ch1)=User_Rate_SOINR1(Ch1)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit1.*H1(Ch1))./(Noise+IntfcO1(Ch1,i)))))),ro_max);
            
            
        else % ISP2 will take the channel
            
            Counter_ISP(2)=Counter_ISP(2)+1;
            B1=B1+Time_Sharing_weight(1); % Updating the bucket of ISP1
            B2=B2+Time_Sharing_weight(2)-1; % Updating the bucket of ISP2
            b1=bb1; % Retrieving the buckets values for the users of ISP1
            Counter1=bc1; % Retrieving the counter values for the users of ISP1
            Rate_ISP2=Rate_ISP2+sum(Rate2);
            User_Rate2(Ch2)=User_Rate2(Ch2)+Rate2;
            User_SINR2(Ch2)=User_SINR2(Ch2)+((P_transmit2*H2(Ch2))./(Noise+IntfcI2(Ch2)+IntfcO2(Ch2,i)));
            
            User_Rate_SNR2(Ch2)=User_Rate_SNR2(Ch2)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit2.*H2(Ch2))./(Noise))))),ro_max);
            User_Rate_SIINR2(Ch2)=User_Rate_SIINR2(Ch2)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit2.*H2(Ch2))./(Noise+IntfcI2(Ch2)))))),ro_max);
            User_Rate_SOINR2(Ch2)=User_Rate_SOINR2(Ch2)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit2.*H2(Ch2))./(Noise+IntfcO2(Ch2,i)))))),ro_max);
        end
        
    elseif n1>0 && n2==0
        Counter_ISP(1)=Counter_ISP(1)+1;
        Rate_ISP1=Rate_ISP1+sum(Rate1);
        User_Rate1(Ch1)=User_Rate1(Ch1)+Rate1;
        User_SINR1(Ch1)=User_SINR1(Ch1)+((P_transmit1*H1(Ch1))./(Noise+IntfcI1(Ch1)+IntfcO1(Ch1,i)));
        
        User_Rate_SNR1(Ch1)=User_Rate_SNR1(Ch1)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit1.*H1(Ch1))./(Noise))))),ro_max);
        User_Rate_SIINR1(Ch1)=User_Rate_SIINR1(Ch1)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit1.*H1(Ch1))./(Noise+IntfcI1(Ch1)))))),ro_max);
        User_Rate_SOINR1(Ch1)=User_Rate_SOINR1(Ch1)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit1.*H1(Ch1))./(Noise+IntfcO1(Ch1,i)))))),ro_max);
        
    elseif n2>0 && n1==0
        
        Counter_ISP(2)=Counter_ISP(2)+1;
        Rate_ISP2=Rate_ISP2+sum(Rate2);
        User_Rate2(Ch2)=User_Rate2(Ch2)+Rate2;
        User_SINR2(Ch2)=User_SINR2(Ch2)+((P_transmit2*H2(Ch2))./(Noise+IntfcI2(Ch2)+IntfcO2(Ch2,i)));
        
        User_Rate_SNR2(Ch2)=User_Rate_SNR2(Ch2)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit2.*H2(Ch2))./(Noise))))),ro_max);
        User_Rate_SIINR2(Ch2)=User_Rate_SIINR2(Ch2)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit2.*H2(Ch2))./(Noise+IntfcI2(Ch2)))))),ro_max);
        User_Rate_SOINR2(Ch2)=User_Rate_SOINR2(Ch2)+min(((1-OvheadLoss)*log2(1+(Lossfact*((P_transmit2.*H2(Ch2))./(Noise+IntfcO2(Ch2,i)))))),ro_max);
        
    end
    
    % Reseting the parameters
    Th1=[];
    C1=[];
    Ch1=[];
    k1=1;
    j1=1;
    Th2=[];
    C2=[];
    Ch2=[];
    k2=1;
    j2=1;
    
end

if n1>0 && n2>0
    
    User_Share=[Counter1;Counter2]./T_slot;
    User_per_slot=[sum(Counter1);sum(Counter2)]./T_slot;
    Rate_ISP=[Rate_ISP1;Rate_ISP2]./T_slot;
    User_Rate=[User_Rate1;User_Rate2]./T_slot;
    Counter_ISP=Counter_ISP./T_slot;
    
    User_Rate_SNR=[User_Rate_SNR1;User_Rate_SNR2]./T_slot;
    User_Rate_SIINR=[User_Rate_SIINR1;User_Rate_SIINR2]./T_slot;
    User_Rate_SOINR=[User_Rate_SOINR1;User_Rate_SOINR2]./T_slot;
    
    User_SINR=([User_SINR1;User_SINR2]./T_slot)./User_Share;
    User_SINR_All=[User_SINR1_All;User_SINR2_All]./T_slot;
    User_SNR_All=[User_SNR1_All;User_SNR2_All]./T_slot;
    User_INR_All=[User_INR1_All;User_INR2_All]./T_slot;
    User_IINR_All=[User_IINR1_All;User_IINR2_All]./T_slot;
    User_OINR_All=[User_OINR1_All;User_OINR2_All]./T_slot;
    
elseif n1>0 && n2==0
    
    User_Share=Counter1./T_slot;
    User_per_slot=[sum(Counter1),0]./T_slot;
    Rate_ISP=[Rate_ISP1,0]./T_slot;
    User_Rate=User_Rate1./T_slot;
    Counter_ISP=Counter_ISP./T_slot;
    
    User_Rate_SNR=User_Rate_SNR1./T_slot;
    User_Rate_SIINR=User_Rate_SIINR1./T_slot;
    User_Rate_SOINR=User_Rate_SOINR1./T_slot;
    
    User_SINR=(User_SINR1./T_slot)./User_Share;
    User_SINR_All=User_SINR1_All./T_slot;
    User_SNR_All=User_SNR1_All./T_slot;
    User_INR_All=User_INR1_All./T_slot;
    User_IINR_All=User_IINR1_All./T_slot;
    User_OINR_All=User_OINR1_All./T_slot;
    
elseif n2>0 && n1==0
    
    User_Share=Counter2./T_slot;
    User_per_slot=[0,sum(Counter2)]./T_slot;
    Rate_ISP=[0,Rate_ISP2]./T_slot;
    User_Rate=User_Rate2./T_slot;
    Counter_ISP=Counter_ISP./T_slot;
    
    User_Rate_SNR=User_Rate_SNR2./T_slot;
    User_Rate_SIINR=User_Rate_SIINR2./T_slot;
    User_Rate_SOINR=User_Rate_SOINR2./T_slot;
    
    User_SINR=(User_SINR2./T_slot)./User_Share;
    User_SINR_All=User_SINR2_All./T_slot;
    User_SNR_All=User_SNR2_All./T_slot;
    User_INR_All=User_INR2_All./T_slot;
    User_IINR_All=User_IINR2_All./T_slot;
    User_OINR_All=User_OINR2_All./T_slot;
end

end