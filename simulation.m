p = 0.5;

uedens=500;

flag1='00';
flag3='1110001';
flag4='0';
d = 181;
antenna=1;
noise=-77;
bsdens=100;

setenv('FLAG5', num2str(antenna));
setenv('UEDENSITY', num2str(uedens));
setenv('BSDENSITY', num2str(bsdens));
setenv('NOISE',  num2str(noise));
setenv('THETA',  num2str(d));
setenv('PERCENT',num2str(p));
setenv('FLAG1', flag1);
setenv('FLAG3', flag3);
setenv('FLAG4', flag4);
setenv('SIMWINDOW', num2str(1000));

Network_Supervisor(1);

