AI = getenv('SLURM_ARRAY_TASK_ID');
NAI = str2num(AI);

uedens=500;
bsdens=100;

flag1='10'; % change to '00' for mmWave
flag3='10100011';
flag4='0';
d = 181;
antenna=1;
noise=-87+4.771213; % change to -77 for mmWave

frf=3; % change to 1 for mmWave

for p = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.65, 0.675, 0.7, 0.725, 0.75, 0.8, 0.9]

   setenv('FLAG5', num2str(antenna));
   setenv('UEDENSITY', num2str(uedens));
   setenv('BSDENSITY', num2str(bsdens));
   setenv('NOISE',  num2str(noise));
   setenv('THETA',  num2str(d));
   setenv('PERCENT',num2str(p));
   setenv('FLAG1', flag1);
   setenv('FLAG3', flag3);
   setenv('FLAG4', flag4);
   setenv('SIMWINDOW', num2str(1000));
   setenv('NOFTS', num2str(p));
   setenv('NPRS', num2str(p));
   setenv('FRF', num2str(frf));
   Network_Supervisor(NAI);

end

