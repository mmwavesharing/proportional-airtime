function [N,BS_Locx,BS_Locy,BS_ISP_share,BS_ISP_part]=BS_Deployment(flag2,N_ISP,r_clus,N_BS,p,Sim_win)

if strcmp(flag2,'1')  % With spatial correlation
    
    % Placing BSs of ISP1 based on hPPP model
    N=zeros(N_ISP,1);
    while N(1)==0 || N(2)==0
        N(1)=random('poisson',N_BS(1));
        S=rand([N(1),1])<=p;
        N(2)=sum(S);
    end
    BS_Locx=zeros(sum(N),1);
    BS_Locy=zeros(sum(N),1);
    BS_Locx(1:N(1))=-(Sim_win/2)+Sim_win*rand([N(1),1]);
    BS_Locy(1:N(1))=-(Sim_win/2)+Sim_win*rand([N(1),1]);
    
    % Placing a BS for ISP2 with probability p in the R vicinity of the BS of ISP1 randomly at uniform
    for i=1:N(1)
        x=2*Sim_win;
        y=2*Sim_win;
        while x>Sim_win/2 || x<-Sim_win/2 || y>Sim_win/2 || y<-Sim_win/2
            R=r_clus*sqrt(rand(1,1));
            phi=2*pi*rand(1,1);
            x=S(i).*(BS_Locx(i)+R.*cos(phi));
            y=S(i).*(BS_Locy(i)+R.*sin(phi));
            Ax(i)=x;
            Ay(i)=y;
        end
    end
    BS_Locx(N(1)+1:sum(N))=Ax(Ax~=0);
    BS_Locy(N(1)+1:sum(N))=Ay(Ay~=0);
    % BS_ISP relationship in BS partitioning case
    BS_ISP_part=zeros(sum(N),2);
    BS_ISP_part(1:N(1),1)=ones(N(1),1);
    BS_ISP_part(N(1)+1:sum(N),2)=ones(N(2),1);
    
    % BS_ISP relationship in BS sharing case
    BS_ISP_share=ones(sum(N),2);
    
    
elseif strcmp(flag2,'0') % Without spatial correlation
    
    % Placing BSs of ISPs based on hPPP model
    N=zeros(N_ISP,1);
    while N(1)==0 || N(2)==0
        N(1)=random('poisson',N_BS(1));
        N(2)=random('poisson',N_BS(2));
    end
    BS_Locx=zeros(sum(N),1);
    BS_Locy=zeros(sum(N),1);
    BS_Locx(1:N(1))=-(Sim_win/2)+Sim_win*rand([N(1),1]);
    BS_Locy(1:N(1))=-(Sim_win/2)+Sim_win*rand([N(1),1]);
    BS_Locx(N(1)+1:sum(N))=-(Sim_win/2)+Sim_win*rand([N(2),1]);
    BS_Locy(N(1)+1:sum(N))=-(Sim_win/2)+Sim_win*rand([N(2),1]);
    
    % BS_ISP relationship in BS partitioning case
    BS_ISP_part=zeros(sum(N),2);
    BS_ISP_part(1:N(1),1)=ones(N(1),1);
    BS_ISP_part(N(1)+1:sum(N),2)=ones(N(2),1);
    
    % BS_ISP relationship in BS sharing case
    BS_ISP_share=ones(sum(N),2);
    
else
    
    disp('Error occured in base station deployment procedure, BS deployment mode is not available')
    
end

end
