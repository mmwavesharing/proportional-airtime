function Network_Visualization(N,TargetBSx_share,TargetBSx_part_1,TargetBSx_part_2,TargetBSy_share,TargetBSy_part_1,TargetBSy_part_2,TargetBS_id_share,TargetBS_id_part_1,TargetBS_id_part_2,BS_Locx,BS_Locy,BS_ISP_share,BS_ISP_part,n,UE_Locx,UE_Locy,UE_ISP,UE_BS_share,UE_BS_part,Sim_win)

if N(1)>2 && N(2)>2
    
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(1,2,1) % Without BS sharing 
    
    plot(BS_Locx(BS_ISP_part(:,1)==1),BS_Locy(BS_ISP_part(:,1)==1),'or','LineWidth',6);
    hold on
    plot(BS_Locx(BS_ISP_part(:,2)==1),BS_Locy(BS_ISP_part(:,2)==1),'ok','LineWidth',6);
    hold on
    plot(UE_Locx(1:n(1)),UE_Locy(1:n(1)),'or','LineWidth',1);
    hold on
    plot(UE_Locx(n(1)+1:sum(n)),UE_Locy(n(1)+1:sum(n)),'ok','LineWidth',1)
    hold on
    voronoi(BS_Locx(BS_ISP_part(:,1)==1),BS_Locy(BS_ISP_part(:,1)==1),'r');
    hold on
    voronoi(BS_Locx(BS_ISP_part(:,2)==1),BS_Locy(BS_ISP_part(:,2)==1),'k');
    hold on
    plot(TargetBSx_part_1,TargetBSy_part_1,'vg','LineWidth',3);
    hold on
    plot(UE_Locx(UE_BS_part(:,TargetBS_id_part_1)==1),UE_Locy(UE_BS_part(:,TargetBS_id_part_1)==1),'xr','LineWidth',2)
    hold on
    plot(TargetBSx_part_2,TargetBSy_part_2,'vg','LineWidth',3);
    hold on
    plot(UE_Locx(UE_BS_part(:,TargetBS_id_part_2)==1),UE_Locy(UE_BS_part(:,TargetBS_id_part_2)==1),'xk','LineWidth',2)
    
    title('Without BS Sharing','Fontsize',16)
    set(gca,'Fontsize',12)
    axis([-Sim_win/2 Sim_win/2 -Sim_win/2 Sim_win/2])
    
    
    
    
    subplot(1,2,2) % With BS sharing 
    
    plot(BS_Locx(BS_ISP_share(:,1)==1),BS_Locy(BS_ISP_share(:,1)==1),'ob','LineWidth',6);
    hold on
    plot(BS_Locx(BS_ISP_share(:,2)==1),BS_Locy(BS_ISP_share(:,2)==1),'ob','LineWidth',6);
    hold on
    plot(UE_Locx(1:n(1)),UE_Locy(1:n(1)),'or','LineWidth',1);
    hold on
    plot(UE_Locx(n(1)+1:sum(n)),UE_Locy(n(1)+1:sum(n)),'ok','LineWidth',1)
    hold on
    voronoi(BS_Locx,BS_Locy,'b');
    hold on
    plot(TargetBSx_share,TargetBSy_share,'vg','LineWidth',3);
    hold on
    plot(UE_Locx(UE_BS_share(:,TargetBS_id_share)==1),UE_Locy(UE_BS_share(:,TargetBS_id_share)==1),'xk','LineWidth',2)
    
    title('With BS Sharing','Fontsize',16)
    set(gca,'Fontsize',12)
    axis([-Sim_win/2 Sim_win/2 -Sim_win/2 Sim_win/2])
    
    
else
    
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(1,2,1) % BS partitioning case
    
    plot(BS_Locx(BS_ISP_part(:,1)==1),BS_Locy(BS_ISP_part(:,1)==1),'or','LineWidth',6);
    hold on
    plot(BS_Locx(BS_ISP_part(:,2)==1),BS_Locy(BS_ISP_part(:,2)==1),'ok','LineWidth',6);
    hold on
    plot(UE_Locx(1:n(1)),UE_Locy(1:n(1)),'or','LineWidth',1);
    hold on
    plot(UE_Locx(n(1)+1:sum(n)),UE_Locy(n(1)+1:sum(n)),'ok','LineWidth',1)
    hold on
    plot(TargetBSx,TargetBSy,'vg','LineWidth',3);
    hold on
    plot(UE_Locx(UE_BS_part(:,TargetBS_id)==1),UE_Locy(UE_BS_part(:,TargetBS_id)==1),'xg','LineWidth',2)
 
    title('Without BS Sharing','Fontsize',16)
    set(gca,'Fontsize',12)
    axis([-Sim_win/2 Sim_win/2 -Sim_win/2 Sim_win/2])
    
    
    
    subplot(1,2,2) % BS sharing case
    
    plot(BS_Locx(BS_ISP_share(:,1)==1),BS_Locy(BS_ISP_share(:,1)==1),'ob','LineWidth',6);
    hold on
    plot(BS_Locx(BS_ISP_share(:,2)==1),BS_Locy(BS_ISP_share(:,2)==1),'ob','LineWidth',6);
    hold on
    plot(UE_Locx(1:n(1)),UE_Locy(1:n(1)),'or','LineWidth',0.5);
    hold on
    plot(UE_Locx(n(1)+1:sum(n)),UE_Locy(n(1)+1:sum(n)),'ok','LineWidth',1)
    hold on
    plot(TargetBSx,TargetBSy,'vg','LineWidth',3);
    hold on
    plot(UE_Locx(UE_BS_share(:,TargetBS_id)==1),UE_Locy(UE_BS_share(:,TargetBS_id)==1),'xg','LineWidth',2)
 
    
    
    title('With BS Sharing','Fontsize',16)
    set(gca,'Fontsize',12)
    axis([-Sim_win/2 Sim_win/2 -Sim_win/2 Sim_win/2])
    
end





end