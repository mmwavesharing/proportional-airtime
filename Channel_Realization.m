function [H]=Channel_Realization(n,T_slot,D_UE_BS,UE_BS,PrLOS,PrOUT,plLOS,plNLOS,sigma_LOS,sigma_NLOS)

% Stochastic channel parameters
ShadowLOS=randn(sum(n),T_slot);
ShadowNLOS=randn(sum(n),T_slot);
Rayl=random('Rayleigh',1,[sum(n),T_slot]);
OutageTest=rand(sum(n),T_slot);
LOSTest=rand(sum(n),T_slot);

D_UE_BS_2=D_UE_BS';
dist=D_UE_BS_2(UE_BS'==1);
Outage_State=OutageTest<(PrOUT(dist)*ones(1,T_slot));  % If an element of this vecor is 1 then the correspnding user is in outage
Outage_State=Outage_State*-1e100;
Channel_State=LOSTest<((PrLOS(dist)./(1-PrOUT(dist)))*ones(1,T_slot)); % If an element of this vecor is 1 then the correspnding user has LOS channel; otherwise it has a NLOS channel.
PL=(((plLOS(dist)*ones(1,T_slot))+(sigma_LOS.*ShadowLOS)).*Channel_State.*(1-Outage_State))+(((plNLOS(dist)*ones(1,T_slot))+(sigma_NLOS.*ShadowNLOS)).*(1-Channel_State).*(1-Outage_State)); % Pathloss + shadowing
PL(PL>1e50)=inf;
H=(Rayl.^2)./(10.^(PL./10)); % Fading+shadowing+pathloss  AND LOS/NLOS/OUTAGE

end