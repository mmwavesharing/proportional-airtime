
rm(list=ls(all=TRUE))

library(data.table)



folders <- c("dat-user")

for (folder in folders) {
    setwd(folder)

    files <- c(Sys.glob("User_*.csv"))
#fvals <- unlist(strsplit(folder, "-"))
#names(fvals) <- c("dat", "beamwidth", "sidelobe", "noise")

for (file in files){
    data <- try(fread(file, sep=",", header=TRUE))
    if(class(data)!='try-error') { 

        file <- unlist(strsplit(file, ".c"))[1]
        vals <- unlist(strsplit(file, "_"))
        names(vals) <- c("type", "ISP1share", "ISP2share",
            "bs", "ue", "degree", "noise", "flag3","flag5", 
            "ISPFlag", "nOFTS", "nPRS", "simInd")
        names(data) <- c("simInd", "uid", "variable", "value")
        data$ISPFlag <- vals["ISPFlag"]
        data$noise <- vals["noise"]
        data$bs <- vals["bs"]
	data$antenna <- vals["flag5"]
	data$degree <- vals["degree"]
	data$isp1 <- vals["ISP1share"]
	data$isp2 <- vals["ISP2share"]
	data$isp1OFTS <- vals["nOFTS"]
	data$isp1PRS <- vals["nPRS"]
        ifelse( any("ueCombined" %in% ls()),
            ueCombined <- rbind(ueCombined, data),
            ueCombined <- data
            )
    }
}
#setwd(homedir)
}

ueCombined$ISPFlag <- as.factor(ueCombined$ISPFlag)
ueCombined$noise <- as.numeric(ueCombined$noise)
ueCombined$bs <- as.numeric(ueCombined$bs)
ueCombined$bw <- 10^((ueCombined$noise-7+174)/10.0)
ueCombined$degree <- as.numeric(ueCombined$degree)
ueCombined$antenna <- as.numeric(ueCombined$antenna)
ueCombined$isp1 <- as.numeric(ueCombined$isp1)
ueCombined$isp2 <- as.numeric(ueCombined$isp2)
ueCombined$isp1OFTS <- as.numeric(ueCombined$isp1OFTS)
ueCombined$isp1PRS <- as.numeric(ueCombined$isp1PRS)
ueCombined$variable <- as.factor(ueCombined$variable)

saveRDS(ueCombined, "../datUser.Rda")

rm(data)
#rm(ueCombined)

